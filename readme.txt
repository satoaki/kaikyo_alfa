[命名規則]10/19
多分表が見にくい&もっといい命名規則があると思うので、
各自α版までに提案&ReadMeの変更お願いします(変える前に報告、変えた後も報告お願いします！)



hoge


主な命名規則...


クラス:(ハンガリアン記法)
C+名前
例:CActionModel	CBoxCreate

インターフェース:(ハンガリアン記法)
I+名前
例:ICommand IOperation

抽象クラス(パスカル型)
パスカル型+先頭にAbstractか、末尾にBase
例:ShowaBase  AbstractShowa

列挙型:(パスカル型)
パスカル型+Kindがいいっぽい
例:WindowShowKind FruitShowKind

メソッド:(パスカル型)
純粋なパスカル型...もっといいのがありそう
例:Accept  CreateCommand

コルーチン:(パスカル型)
IE + パスカル型
例:IEWait IEFade

プロパティ:(パスカル型)
純粋なパスカル型
例:UserName  HasItem

フィールド:(ハンガリアン+パスカル型orキャメル型)
先頭に_を付ける
例:_userName  _TextBox

引数:(キャメル型)
純粋なキャメル型
例:text　gameObject

ローカル変数(ハンガリアン記法)
変数種別(小文字)+名前
例:boolのFlag→bFlag	intのCount→iCount等々

グローバル変数(ハンガリアン記法)
g_+名前(ローカル変数と同じ)
例:g_bFlag  g_iCount



補足...

パスカル型
単語の頭文字を大文字にする
例:PlayerAsset

キャメル型
最初以外の単語を大文字にする
例:windowSetup	createMenu

スネーク型
全部大文字！
例:BACK_GROUND_COLOR


ハンガリアン記法
接頭詞+変数名。Pay（お金）にyen(日本円)やdol(ドル)などを付けて、変数名に細かく情報を入れる
例:yenPay dolPay





参照...
http://oxynotes.com/?p=8679(主にハンガリアン記法で綺麗な図があります)
http://www.caferatnica.jp/programming/naming.aspx
http://blog.goo.ne.jp/go5oyaji/e/64814d14f7126045901bb8a202577a59