﻿using UnityEngine;
using System.Collections;

public class LoseResult : MonoBehaviour
{
    float fog = 0f;
    [SerializeField]
    private float LoseTime;
    private SpriteRenderer spRenderer;
    private float fade;

    void Start()
    {
        RenderSettings.fog = false;
        RenderSettings.fogDensity = 0.0f;
    }

    void Update ()
    {
        //だいたい１秒ごとに処理
        LoseTime -= Time.deltaTime;
      
        if(LoseTime <= 0.0f)
        {
            Object.Destroy(gameObject);
            RenderSettings.fog = true;
        }

        if(RenderSettings.fog)
        {
            RenderSettings.fogColor = Color.red;
            RenderSettings.fogDensity = 0.1f;
         
        }
        
	}
}
