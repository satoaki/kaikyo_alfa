﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Fade : MonoBehaviour
{

    //置き換え
    Image _image;
    private float fadeA;
    [SerializeField, Range(0.01f, 0.5f),
        Tooltip("フェード切り替えの早さ")]
    private float fadeSpeed;
    void Awake()
    {
        _image = GetComponent<Image>();
        fadeA = 1;
    }

    //フェードイン
    public bool FadeIn(float r, float g, float b)
    {
        //色を置き換える
        _image.color = new Color(r, g, b, fadeA);

        //徐々に透過度を下げる
        fadeA -= fadeSpeed;

        //一定以上になったら
        if (fadeA <= 0)
        {
            return true;
        }
        return false;
    }

    //フェードアウト
    public bool FadeOut(float r, float g, float b)
    {
        _image.color = new Color(r, g, b, fadeA);

        fadeA += fadeSpeed;
        if (fadeA >= 1)
        {
            return true;
        }

        return false;

    }
}
