﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CheckWinner : MonoBehaviour {

    CPlayerFade playerFade;

    [SerializeField]
    private GameObject MainCamera;
    [SerializeField]
    //private GameObject PCCamera;

    private GameObject winpanelObj = null;
    private GameObject losepanelObj = null;

    private string OculusWinCheck;

    // Use this for initialization
    void Awake () {

        OculusWinCheck = PlayerPrefs.GetString("OculusWinnerSave");

        winpanelObj = GameObject.Find("Win");
        winpanelObj = GameObject.Find("Lose");

        if (CTopMenu.sSelectDevice == "Oculus")
        {
            if (OculusWinCheck == "Win")
            {
                winpanelObj.SetActive(false);
            }
            else if (OculusWinCheck == "Lose")
            {
                losepanelObj.SetActive(false);
                //PCCamera.transform.localPosition = winResult;
                //SceneManager.LoadScene("Title");
            }
        }
        else if (CTopMenu.sSelectDevice == "PC")
        {
            if (OculusWinCheck == "Win")
            {
                winpanelObj.SetActive(false);
            }
            else if (OculusWinCheck == "Lose")
            {
                losepanelObj.SetActive(false);
                //PCCamera.transform.localPosition = winResult;
                //SceneManager.LoadScene("Title");
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("ButtonBPC") || Input.GetKeyDown("space"))
        {
            PlayerPrefs.DeleteKey("OculusWinnerSave");
            Debug.Log(OculusWinCheck);

            SceneManager.LoadScene("Title");
            //Application.LoadLevel("Title");
        }
        
    }
}
