﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CTutorialScript : MonoBehaviour
{

    private GameObject Reibaisi_sprite;
    private GameObject Ghostside_sprite;

    [SerializeField]
    private Text text;
    private Color _defaultWhite = new Color(1f, 1f, 1f, 1f);
    private Color _waitingRed = new Color(1f, 0.8f, 0.8f, 1f);

    void Awake()
    {
        Reibaisi_sprite = GameObject.Find("説明画面_霊媒師");
        Ghostside_sprite = GameObject.Find("説明画面_怨霊");

        text.text = "";
        if (CTopMenu.sSelectDevice == "PC")
        {
            Destroy(Reibaisi_sprite);
        }
        if (CTopMenu.sSelectDevice == "Oculus")
        {
            Destroy(Ghostside_sprite);
            Reibaisi_sprite.GetComponent<SpriteRenderer>().color = _defaultWhite;
            
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButton("Fire1"))
        {
            Reibaisi_sprite.GetComponent<SpriteRenderer>().color = _waitingRed;
            text.text = "Waiting...";
        }


    }
}
