﻿using UnityEngine;
using UnityEngine.UI;//uGUIにアクセス
using System.Collections;

public class fade : MonoBehaviour
{
    private Image Capsule;
    private float time;
    public float fadetime;

    void Start()
    {
        time = fadetime;//初期化
        Capsule = GetComponent<Image>();//Imageコンポネントを取得
    }

    void Update()
    {
        time -= Time.deltaTime;//時間更新(徐々に減らす)
        float a = time / fadetime;//徐々に0に近づける
        var color = Capsule.color;//取得したimageのcolorを取得
        color.a = a;//カラーのアルファ値(透明度合)を徐々に減らす
        Capsule.color = color;//取得したImageに適応させる
    }
}
