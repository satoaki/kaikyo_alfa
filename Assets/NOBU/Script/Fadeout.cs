﻿using UnityEngine;
using System.Collections;

public class Fadeout : MonoBehaviour
{
    public float fadeTime = 1f;

    private float currentRemainTime;
    private SpriteRenderer spRendere;


	void Start ()
    {
        //初期化
        currentRemainTime = fadeTime;
        spRendere = GetComponent<SpriteRenderer>();
	}
	
	void Update ()
    {
        //残り時間
        currentRemainTime -= Time.deltaTime;
        
        //フェードアウト
        float alpha = currentRemainTime / fadeTime;
        var color = spRendere.color;
        color.a = alpha;
        spRendere.color = color;

	}
}
