﻿using UnityEngine;
using System.Collections;

public class Sin_Cos : MonoBehaviour
{
    public float yurayura = .2f;

    void Update()
    {

        transform.position = new Vector3(10 * Mathf.Cos
            (Time.frameCount * yurayura / 4)
         , 5 * (Mathf.Cos(Time.frameCount * yurayura / 2)),
         transform.position.z);
    }

}
