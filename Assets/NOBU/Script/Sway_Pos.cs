﻿using UnityEngine;
using System.Collections;

public class Sway_Pos : MonoBehaviour
{

    [SerializeField]
    private GameObject camera;

    void Start()
    {
        transform.position = camera.transform.position;
        transform.Translate(0, 2, 5, Space.World);
    }
}
