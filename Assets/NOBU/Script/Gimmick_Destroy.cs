﻿using UnityEngine;
using System.Collections;

public class Gimmick_Destroy : MonoBehaviour
{
    public float destroyTime = 1f;

    private float currentRemainTime;
    private SpriteRenderer spRenderer;

    void Start()
    {
        //初期化
        currentRemainTime = destroyTime;
        spRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        //残り時間を更新
        currentRemainTime -= Time.deltaTime;

        if (currentRemainTime <= 0f)
        {
            //残り時間が無くなったらギミック消滅
            GameObject.Destroy(gameObject);
            return;
        }
    }

}
