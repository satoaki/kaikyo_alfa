﻿using UnityEngine;
using System.Collections;
using System;

public class CinItemVase : MonoBehaviour {

    [SerializeField]//チュートリアルふすまを入れる
    GameObject _tutorialFusuma;

    [SerializeField] //アイテムの画像を入れる
    GameObject ItemSprite = null;
    [SerializeField]
    string ItemName;

    [SerializeField]
    GameObject emission;

    ParticleSystem part;
   
    GameObject _item;

    void Start ()
    {
        part = emission.GetComponent<ParticleSystem>();
 	}
	
	void Update () {

        if(part.isPlaying && Input.GetButton("Fire1"))
        {

            StartCoroutine(Stay());
           
        }
	
	}

    private IEnumerator Stay()
    {
        Vector3 Itempos = new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z);
        _item = Instantiate(ItemSprite, Itempos, transform.rotation) as GameObject;
        _item.transform.parent = emission.transform;

        yield return new WaitForSeconds(2.0f);
        if(ItemName == "Juzu")
        {
            GameObject.Find("GameController").SendMessage("Juzu_get");
            _tutorialFusuma.GetComponent<CTurtreal_Fusuma>().g_bTurtrealCLear = true;
        }
        else if(ItemName == "Ohuda")
        {
            GameObject.Find("GameController").SendMessage("Ohuda_get");
        }
        else if(ItemName == "Haraibou")
        {
            GameObject.Find("GameController").SendMessage("Haraibou_get");
        }

        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
           emission.SendMessage("PStart");
        }
    }

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            emission.SendMessage("PEnd");
        }
    }
}
