﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class CItemManeger : NetworkBehaviour {

    //エディター画面でもアイテム情報を受け取れるようにプロパティで受け取り専用
    private bool g_bJuzu; 
    public bool Juzu { get { return g_bJuzu; } }
    private bool g_bOhuda;
    public bool Ohuda { get { return g_bOhuda; } }
    private bool g_bHaraibou;
    public bool Haraibou { get { return g_bHaraibou; } }
    [SerializeField]
    private bool g_bClearflug;
    public bool Clearflug { get { return g_bClearflug; } }
    [SerializeField]
    private GameObject BossFusuma;

    void Awake()
    {
        g_bJuzu = false;
        g_bOhuda = false;
        g_bHaraibou = false;
        g_bClearflug = false;
    }
	
    /// <summary>
    /// 以下Playerが手に入れたらboolを切り替える
    /// </summary>
    public void Juzu_get()
    {
        g_bJuzu = true;
    }

    public void Ohuda_get()
    {
        g_bOhuda = true;
    }
    
    public void Haraibou_get()
    {
        g_bHaraibou = true;
    }


    void Update()
    {
        //デバッグキー, P + Zでアイテム全取得
        if(Input.GetKey("p") && Input.GetKeyDown("z"))
        {
            g_bJuzu = true;
            g_bOhuda = true;
            g_bHaraibou = true;

        }

        if (g_bJuzu && g_bOhuda && g_bHaraibou)
        {
            if (!g_bClearflug)
            {
                g_bClearflug = true;
                BossFusuma.SendMessage("FusumaStart");
            }

        }
    }
}
