﻿using UnityEngine;
using System.Collections;

public class CTurtreal_Fusuma : FusumaMove {

    public bool g_bTurtrealCLear
    {
        get; set;
    }

    [SerializeField]
    private GameObject Player;
    
	void Start ()
    {
        g_bTurtrealCLear = false;
	}
	
	void Update ()
    {
	    if(Player.GetComponent<CItemManeger>().Juzu)
        {
            Debug.Log("Tutorial_fusuma");
            base.FusumaUpdate();
        }
	}
}
