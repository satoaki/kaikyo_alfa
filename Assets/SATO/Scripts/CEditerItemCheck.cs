﻿using UnityEngine;
using System.Collections;

public class CEditerItemCheck : MonoBehaviour
{

    [SerializeField]
    GameObject Juzu;
    [SerializeField]
    GameObject Ohuda;
    [SerializeField]
    GameObject Haraibou;

    [SerializeField]
    GameObject _player;


    void Start()
    {
        Juzu.SetActive(false);
        Ohuda.SetActive(false);
        Haraibou.SetActive(false);
    }

    void Update()
    {
        if (_player.GetComponent<CItemManeger>().Juzu)
        {
            Juzu.SetActive(true);
        }

        if (_player.GetComponent<CItemManeger>().Ohuda)
        {
            Ohuda.SetActive(true);
        }

        if (_player.GetComponent<CItemManeger>().Haraibou)
        {
            Haraibou.SetActive(true);
        }

    }
}
