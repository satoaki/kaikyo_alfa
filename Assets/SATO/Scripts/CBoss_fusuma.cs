﻿using UnityEngine;
using System.Collections;
using System;

public class CBoss_fusuma : FusumaMove {

    [SerializeField]
    private bool g_bfusumaLock;
    [SerializeField]
    private GameObject _Player;

    private bool g_bClearflug = false;
    
	void Start ()
    {
        //ロックされてる状態 
        g_bfusumaLock = true;
    }

    private IEnumerator UpdateFusuma()
    {
        if (_Player.GetComponent<CItemManeger>().Clearflug == true)
        {
            g_bfusumaLock = false;
        }
        yield return null;

    }

    void FusumaStart()
    {
        StartCoroutine(UpdateFusuma());
    }

    void Update ()
    {
        if(!g_bfusumaLock)base.FusumaUpdate();
        ClearFlug();
        
	}
    
    void ClearFlug()
    {
        //、デバッグキー P + X + C
        if (Input.GetKey("p") && Input.GetKey("x") && Input.GetKeyDown("c"))
        {
            GameObject.Find("GameController").SendMessage("OculusWin");
        }

        if (!g_bfusumaLock && base.g_bmove)
        {
            GameObject.Find("GameController").SendMessage("OculusWin");
        }
    }

}
