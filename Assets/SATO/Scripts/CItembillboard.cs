﻿using UnityEngine;
using System.Collections;

public class CItembillboard : MonoBehaviour {

    [SerializeField]
    private Camera TargetCamera;

	void Start ()
    {
	    if(TargetCamera == null)
        {
            TargetCamera = GameObject.Find("OculusCamera").GetComponent<Camera>();
        }
	}
	
	void Update ()
    {
        transform.LookAt(TargetCamera.transform.position);
	}
}
