﻿using UnityEngine;
using System.Collections;

public class CReferee : MonoBehaviour
{
    //[SerializeField]
    CPlayerFade Fade;

    void Start()
    {
        //Fade = GameObject.Find("Fade").GetComponent<CPlayerFade>();
        Fade = transform.GetComponent<CPlayerFade>();
    }

    void OculusWin()
    {
        StartCoroutine(WinResult());
    }

    void OculusLose()
    {
        //負けた時の処理
        Fade.Gameoverflug = true;
    }

    IEnumerator WinResult()
    {
        yield return new WaitForSeconds(2.0f);

        Fade.Clearflug = true;
        yield return null;
    }

}
