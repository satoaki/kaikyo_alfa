﻿using UnityEngine;
using System.Collections;


public class CItem : MonoBehaviour
{

    [SerializeField]
    GameObject Player;
    //CItemManeger itemmaneger;
    Renderer render;
    public string g_sItemName;
    void Start()
    {
        render = GetComponent<Renderer>();

    }

    void Update()
    {
        ItemCheck();
    }

    void ItemCheck()
    {
        if (Player != null)
        {
            if (g_sItemName == "Juzu")
            {
                if (Player.GetComponent<CItemManeger>().Juzu)
                {
                    render.material.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    render.material.color = new Color(0, 0, 0, 215.0f / 255);
                }
            }
            else if (g_sItemName == "Ohuda")
            {
                if (Player.GetComponent<CItemManeger>().Ohuda)
                {
                    render.material.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    render.material.color = new Color(0, 0, 0, 230.0f / 255);
                }
            }
            else if (g_sItemName == "Haraibou")
            {
                if (Player.GetComponent<CItemManeger>().Haraibou)
                {
                    render.material.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    render.material.color = new Color(0, 0, 0, 210.0f / 255);
                }
            }

        }
    }

}
