﻿
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.Networking;
using System;

public class CPlayerContoller : NetworkBehaviour
{
    [SerializeField, Range(0.01f, 0.03f), Tooltip("動く速さ")]
    private float g_fMovespeed = 0.02f;

    private AudioSource WalkSE = null;
    float walktime = 0.5f;

    void Awake()
    {
        InputTracking.Recenter(); //オキュラスの向きのリセット
        WalkSE = GetComponent<AudioSource>();
    }

    void Update()
    {
        PlayerMove();
        Rotate();  //オキュラスで試すときはコメントアウト
        //Action();
        //NetworkServer.Spawn(gameObject);
    }

    void PlayerMove()
    {
        if (CTopMenu.sSelectDevice == "Oculus")
        {


            //Oculusつないでるとき以外コメントアウト
            Quaternion rotation = InputTracking.GetLocalRotation(VRNode.Head); //オキュラスの向きをとる
            Vector3 roteuler = rotation.eulerAngles;  //とったクオータニオンをオイラー変換
            Quaternion rot = Quaternion.Euler(0, roteuler.y, 0);

            float move_x = Input.GetAxis("Horizontal");
            float move_z = Input.GetAxis("Vertical");
            Vector3 move_pos = new Vector3(move_x * g_fMovespeed, 0, move_z * g_fMovespeed);

            //rotを消したら体の向きで移動する
            transform.Translate(rot * move_pos);  //Oculus時

            if (move_x != 0 || move_z != 0)
            {
                walktime -= Time.deltaTime;
                if (walktime < 0.0f)
                {
                    WalkSE.PlayOneShot(WalkSE.clip);
                    walktime = 0.5f;
                }
            }
            else
            {
                walktime = 0.5f;
            }
        }
    }

    void Rotate()
    {

        if(CTopMenu.sSelectDevice == "Oculus")
        {
            float mouse_x = Input.GetAxis("Angle");
            if (mouse_x != 0)
            {
                transform.Rotate(0, -mouse_x, 0);
            }
        }

        //float mouse_y = Input.GetAxis("Mouse Y");
        //if (mouse_y != 0)
        //{
        //    transform.Rotate(mouse_y * 5, 0, 0);
        //}


    }

    //void Action()
    //{
    //    if (Input.GetButtonDown("Fire1"))
    //    {
    //        //選択条件を入れて選択決定する
    //    }
    //}

}