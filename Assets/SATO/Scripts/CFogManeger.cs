﻿using UnityEngine;


public class CFogManeger : MonoBehaviour {

    [SerializeField, Range(0.0f, 255.0f), Tooltip("赤")]
    private float g_fRed = 0.0f;
    [SerializeField, Range(0.0f, 255.0f), Tooltip("緑")]
    private float g_fGreen = 0.0f;
    [SerializeField, Range(0.0f, 255.0f), Tooltip("青")]
    private float g_fBlue = 0.0f;
    [SerializeField, Range(0.0f, 1.0f), Tooltip("霧の濃さ")]
    private float g_fDensity = 0.0f;

    public void Fog_Set(int red, int green, int blue, float density)
    {
        g_fRed = (red < 256) ? red : 255;
        g_fGreen = (green < 256) ? g_fGreen : 255;
        g_fBlue = (blue < 256) ? g_fBlue : 255;
        g_fDensity = (density <=  1.0f) ? g_fDensity : 1.0f;
    }

    void Start ()
    {
        RenderSettings.fog = true;
        RenderSettings.fogDensity = g_fDensity;
        RenderSettings.fogColor = new Color(g_fRed / 255.0f, g_fGreen / 255.0f, g_fBlue / 255.0f);
	}
	
	void Update ()
    {
        RenderSettings.fogDensity = g_fDensity;
        RenderSettings.fogColor = new Color(g_fRed / 255.0f, g_fGreen / 255.0f, g_fBlue / 255.0f);

        //Fogの色や濃さを変えるのはここで行う
    }
}
