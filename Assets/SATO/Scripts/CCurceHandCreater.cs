﻿using UnityEngine;
using System.Collections;

public class CCurceHandCreater : MonoBehaviour {

    //CTimekeeper timekeeper;
    Transform player;
    GameObject _networkManager;
    bool g_bstart = false;
  
    [SerializeField]
    private GameObject Teato_Blue;
    [SerializeField]
    private GameObject Teato_Green;
    [SerializeField]
    private GameObject Teato_Red;

    private float _count;

    int RandomHand = 0;

    private bool Check = false;

	void Start ()
    {
        _networkManager = GameObject.Find("NetworkManager");
        _count = GameObject.Find("GameController").GetComponent<CTime>()._time;
    }
	
	void Update ()
    {
        _count = GameObject.Find("GameController").GetComponent<CTime>()._time;
        if (!Check)
        {
            player = GameObject.Find("Player").GetComponent<Transform>();
            //timekeeper = GameObject.Find("Candle").GetComponent<CTimekeeper>();
        }

        if (/*timekeeper.LimitTime*/_count < 20.0f && !g_bstart)
        {
            g_bstart = true;
            StartCoroutine("Curce");
        }
	}

    void TeatoRandom()
    {
        if(RandomHand == 0)
        {
            PosColorSelect(Teato_Blue);
        }
        else if(RandomHand == 1)
        {
            PosColorSelect(Teato_Green);
        }
        else if(RandomHand == 2)
        {
            PosColorSelect(Teato_Red);
        }
    }

    void PosColorSelect(GameObject HandColor)
    {
        RandomHand = Random.Range(0, 3);
        Vector3 HandPosition = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), 0);
        GameObject CurceHand = Instantiate(HandColor);
        CurceHand.transform.parent = transform;
        CurceHand.transform.localPosition = HandPosition;
        Quaternion rot = player.transform.localRotation;
        Vector3 rotater = rot.eulerAngles;
        CurceHand.transform.localRotation = Quaternion.Euler(rotater.x, rotater.y, Random.Range(0, 360));
    }

    IEnumerator Curce()
    {
        TeatoRandom();
        yield return new WaitForSeconds(10.0f);
        TeatoRandom();
        TeatoRandom();
        yield return new WaitForSeconds(5.0f);
        TeatoRandom();
        TeatoRandom();
        TeatoRandom();
        TeatoRandom();
        yield return new WaitForSeconds(4.5f);
        _networkManager.GetComponent<CNetworkManager>().StopJoin(true);
        while(true)
        {
            TeatoRandom();
            yield return null;
        }

    }
}
