﻿using UnityEngine;
using System.Collections;

public class CItemCollider : MonoBehaviour {

    [SerializeField]
    private GameObject obj;

    [SerializeField]
    private GameObject particleobj;
    private ParticleSystem part;

    
    void Start ()
    {
        part = particleobj.GetComponent<ParticleSystem>();
    }
	
	void Update ()
    {
	    if(part.isPlaying && Input.GetButton("Fire1"))
        {
            obj.SendMessage("GimmickStart");
            part.SendMessage("PEnd");
        }
	}

    void OnTriggerEnter(Collider col)
    {

        if(col.gameObject.tag == "Player")
        {
            part.SendMessage("PStart");
            
        }
    }

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            part.SendMessage("PEnd");
        }
    }
}
