﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CPlayerFade : MonoBehaviour {

    CFade fade;
    CTimekeeper timekeeper;
    //CNetworkManager _networkManager;
    GameObject _networkManager;

    public bool _oculusResultFlag;

    bool g_bGameoverflug = false;
    public bool Gameoverflug
    {
        get { return g_bGameoverflug; }
        set { g_bGameoverflug = value; }
    }
    bool g_bClearflug = false;
    public bool Clearflug
    {
        get { return g_bClearflug; }
        set { g_bClearflug = value; }
    }

    private string OculusWinCheck;

    Color color; 
    /// <summary>
    /// クリアの時は白いフェイド
    /// ゲームオーバーの時は黒いフェイド
    /// </summary>

    void Start ()
    {
        fade = GetComponent<CFade>();
        fade.FadeA = 0.0f;
        _networkManager = GameObject.Find("NetworkManager");
        _oculusResultFlag = false;
    }

    void Update()
    {
        //クリアした時の処理
        if (g_bClearflug)
        {
            if(fade.Fadeout(1, 1, 1))
            {
                OculusWinCheck = "Win";
                PlayerPrefs.SetString("OculusWinnerSave", OculusWinCheck);
                //_oculusResultFlag = true;
                _networkManager.GetComponent<CNetworkManager>().StopJoin(true);    
            //_networkManager.StopJoin(true);
                //SceneManager.LoadScene("Result");
            }
        }

        //ゲームオーバーの処理
        else if(g_bGameoverflug)
        {
            if(fade.Fadeout(0, 0, 0))
            {
                OculusWinCheck = "Lose";
                PlayerPrefs.SetString("OculusWinnerSave", OculusWinCheck);
                //_oculusResultFlag = true;
                _networkManager.GetComponent<CNetworkManager>().StopJoin(true);
                //_networkManager.StopJoin(true);
                //SceneManager.LoadScene("Result");
            }
        }

        
    }
}
