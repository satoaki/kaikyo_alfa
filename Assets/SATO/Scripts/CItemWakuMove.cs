﻿using UnityEngine;
using System.Collections;
using System;

public class CItemWakuMove : MonoBehaviour {

    [SerializeField]
    GameObject _player;
    CItemManeger itemmanager;
    public string g_sItemName;

	void Start ()
    {
        itemmanager = _player.GetComponent<CItemManeger>();
	}
	
	void Update ()
    {
        ItemCheck();
	}

    private void ItemCheck()
    {
        if(g_sItemName == "Juzu")
        {
            if (itemmanager.Juzu)
            {
                transform.localPosition = new Vector3(-1.899998f, 2.199996f, -0.8f);
            }
        }
        else if(g_sItemName == "Ohuda")
        {
            if(itemmanager.Ohuda)
            {
                transform.localPosition = new Vector3(-2.4f, 2.199996f, -0.8f);
            }
        }
        else if(g_sItemName == "Haraibou")
        {
            if(itemmanager.Haraibou)
            {
                transform.localPosition = new Vector3(-2.9f, 2.199996f, -0.8f);
            }
        }
    }
}
