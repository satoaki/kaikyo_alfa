﻿using UnityEngine;
using System.Collections;

public class CParticleManeger : MonoBehaviour {

    ParticleSystem particle;
	void Start ()
    {
        particle = GetComponent<ParticleSystem>();
        particle.Stop();
	}
	
	void Update ()
    {
	    
	}

    void PStart()
    {
        particle.Play();
    }

    void PEnd()
    {
        particle.Stop();
    }
}
