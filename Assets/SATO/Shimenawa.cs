﻿using UnityEngine;
using System.Collections;

public class Shimenawa : MonoBehaviour {

    private CItemManeger _manager = null;

    [SerializeField]
    private string _itemName;
    private bool _itemCheck = false;


	void Start ()
    {
        _manager = GameObject.Find("Player").GetComponent<CItemManeger>();
        
	}
	
	void Update ()
    {
        if(_itemName == "Juzu")
        {
            _itemCheck = _manager.Juzu;
        }
        else if(_itemName == "Ohuda")
        {
            _itemCheck = _manager.Ohuda;
        }
        else if(_itemName == "Haraibou")
        {
            _itemCheck = _manager.Haraibou;
        }

        if(_itemCheck)
        {
            Destroy(this.gameObject);
        }
	}
}
