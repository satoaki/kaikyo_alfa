﻿using UnityEngine;
using System.Collections;
using System;

public class CfusumaGhostUpdate : MonoBehaviour {

    [SerializeField]
    float limit;
    [SerializeField]
    float fg_speed;
    [SerializeField]
    float fg_v;
 
    [SerializeField]
    CfumaGhostCreate gimmickCreate;



    void Awake() {

    }

    void Start()
    {
        gimmickCreate = GameObject.Find("gimmickCreate").GetComponent<CfumaGhostCreate>();
        Destroy(gameObject, limit);
    }

    // Update is called once per frame
    void Update () {

    
            fg_speed += fg_v;
            transform.Translate(0, 0, -fg_speed);
    }

    void OnDestroy()
    {
        gimmickCreate.start = false;
    }

}
