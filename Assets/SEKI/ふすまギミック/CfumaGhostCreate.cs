﻿using UnityEngine;
using System.Collections;




public class CfumaGhostCreate : MonoBehaviour {

    public GameObject gimmick; //生成するゲームオブジェクト


    [SerializeField]
    public bool start;

    // Use this for initialization
    void Start () {
        start = false;
    }



// Update is called once per frame
void Update () {

        if (Input.GetKeyDown(KeyCode.Space) && start == false)
        {
            start = true;
            transform.position = new Vector3(transform.position.x,
            transform.position.y,0.0f);
            Instantiate(gimmick, transform.position, transform.rotation);
        }
	}
}
