﻿using UnityEngine;
using System.Collections;

public class CPawnJapDoll : MonoBehaviour
{

    [SerializeField]
    private GameObject JapaneseDoll;
    private GameObject player;

    private AudioSource BeginEventErea;
    
    //[SerializeField]
    //TextMesh textmesh;

    private Color alpha = new Color(0.0f, 0.0f, 0.0f, 0.1f);
    private bool active = true;

    // Use this for initialization
    void Awake()
    {
        player = GameObject.Find("Player");
        BeginEventErea = GameObject.Find("BeginEventErea").GetComponent<AudioSource>();
        BeginEventErea.clip = Resources.Load("2kagome") as AudioClip;
        BeginEventErea.Stop();
       // textmesh.text = " ";
    }
    
    void SoundStart()
    {
    }

    void GimmickStart()
    {
        if (active)
        {
            StartCoroutine(DollGimmick());
            active = false;
        }
    }

    IEnumerator DollGimmick()
    {
        BeginEventErea.clip = Resources.Load("ushironosyoumen") as AudioClip;
        BeginEventErea.PlayOneShot(BeginEventErea.clip, 1.0f);
        Vector3 spornpos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z - 1.5f);
        GameObject obj = Instantiate(JapaneseDoll, spornpos, player.transform.rotation) as GameObject;
        obj.transform.parent = transform;
     
        yield return new WaitForSeconds(10.0f);

        Destroy(gameObject);

    }


    //public void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log("Hit");
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        if (Input.GetKeyDown(KeyCode.Space)) flag = true;
    //        Debug.Log("Hit");
    //        textmesh.text = "Press SPACE";

    //        //Destroy(this.gameObject);

    //    }
    //}

    //public void OnCollisionExit(Collision collision)
    //{
    //    Debug.Log("Exit Col");
    //    textmesh.text = " ";
    //}
}
