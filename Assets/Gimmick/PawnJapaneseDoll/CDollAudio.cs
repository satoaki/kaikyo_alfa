﻿using UnityEngine;
using System.Collections;

public class CDollAudio : MonoBehaviour {

    AudioSource _audio;

	void Start ()
    {
        _audio = GetComponent<AudioSource>();    
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            Debug.Log("OK");
            _audio.PlayOneShot(_audio.clip, 1.0f);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            _audio.Stop();
        }

    }

}
