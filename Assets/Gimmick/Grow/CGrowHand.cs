﻿using UnityEngine;
using System.Collections;

public class CGrowHand : MonoBehaviour
{
    [SerializeField]
    private Vector3 endpos = Vector3.zero;

    [SerializeField]
    private float growspeed = 0.0f;

    private bool g_bmove = false;
    float angle = 0;

    //初期化
    void Awake()
    {
        endpos =
            transform.localPosition + endpos;

        g_bmove = false;
    }

    // Update is called once per frame
    void Update()
    {
        //判定ではやす
        if (Input.GetKeyDown(KeyCode.Space))
        {
            g_bmove = true;
        }

        if (g_bmove)
        {
            Grow(endpos, growspeed);
        }
    }

    //生えてくる処理
    void Grow(Vector3 endpos,
        float speed)
    {
        transform.position = Vector3.MoveTowards(
            transform.position, endpos,
            speed);

        if (transform.position.x == endpos.x)
        {
            transform.Rotate(
                new Vector3(0, 0, Mathf.Sin(Time.time)*10)
                * Time.deltaTime);
        }
    }
}
