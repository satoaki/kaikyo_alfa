﻿using UnityEngine;
using System.Collections;

public class CGhostRunner : MonoBehaviour
{
    [SerializeField]
    private GameObject _camera = null;

    [SerializeField]
    private GameObject _RunnerObj = null;
    private GameObject _RealObj = null;

    private Vector3 _ghostPos;
    private AudioSource _runAudio;

    [SerializeField, Range(0.01f, 0.1f),
        Tooltip("お化けの移動スピード Defaultは0.05f")]
    private float g_fmovespeed = 0.05f;

    //ベータ用

    private float RepeatTime = 0.1f;

    //初期化
    void Awake()
    {
        _camera =
            GameObject.Find("OculusCamera");
        _ghostPos =
            _RunnerObj.transform.localPosition;
        _runAudio =
            GetComponent<AudioSource>();
    }

    //画面更新
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    StartCoroutine(Create());
        //}
        RepeatTime -= Time.deltaTime;
        if (RepeatTime < 0.0f)
        {
            Debug.Log("Runner");
            RepeatTime = 60.0f;
            StartCoroutine(Create());
        }


        if (_RealObj != null)
        {
            _RealObj.transform.localPosition = _ghostPos;
            _RealObj.transform.localRotation = _camera.transform.localRotation * Quaternion.Euler(0, 180.0f, 0);
            _ghostPos.x -= g_fmovespeed;
            _runAudio.loop = true;

            if (_ghostPos.x <= -5.0f)
            {

                _runAudio.loop = false;
                _runAudio.Stop();
                Destroy(_RealObj);

                Destroy(gameObject);

            }
        }
    }

    public void StartGimmick()
    {
        StartCoroutine(Create());
    }

    IEnumerator Create()
    {
        Debug.Log("Runner");
        //お化けを複製する
        _ghostPos =
       _RunnerObj.transform.localPosition;

        _RealObj = Instantiate(_RunnerObj,
            _RunnerObj.transform.localPosition,
            _RunnerObj.transform.localRotation) as GameObject;
        _runAudio.Play();
        //作ったオブジェクトをカメラの下に
        _RealObj.name = _RunnerObj.name;
        _RealObj.transform.parent =
            _camera.transform;

        yield return null;
    }
}
