﻿using UnityEngine;
using System.Collections;

public class CKokesiMove : MonoBehaviour
{

    [SerializeField]
    private AudioSource _FallAudio;
    [SerializeField]
    private AudioSource _StandAudio;

    ////使えるか使えないか
    //private bool g_bActive;
    //public bool Active
    //{
    //    get { return g_bActive; }
    //    set { g_bActive = value; }
    //}

    void Awake()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Collider>().enabled = false;
        //g_bActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //GameObject player = GameObject.Find("Player");
            //Quaternion playerrot = player.transform.rotation;
            //transform.position = player.transform.position;
            //transform.Translate(playerrot * new Vector3(0, 1.5f, 2.0f));
        }
    }

    void KokesiStart()
    {
        GetComponent<Collider>().enabled = true;
        GetComponent<Rigidbody>().useGravity = true;
        StartCoroutine("StandUp");
    }

    IEnumerator StandUp()
    {
        
        _FallAudio.PlayOneShot(_FallAudio.clip);
        yield return new WaitForSeconds(1.0f);
        yield return new WaitForSeconds(2.0f);
        GetComponent<Rigidbody>().freezeRotation = true;
        _StandAudio.PlayOneShot(_StandAudio.clip);
        transform.rotation = Quaternion.Euler(0, 180, 0);
        yield return new WaitForSeconds(5.0f);
        Destroy(gameObject);
    }
}