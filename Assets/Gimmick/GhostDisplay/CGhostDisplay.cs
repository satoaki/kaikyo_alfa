﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CGhostDisplay : MonoBehaviour
{ 

    //カメラ
    private GameObject _PlayerObj = null;

    
    public GameObject _fadeObj = null;

    //声
    private AudioSource _voiceSorce = null;

    //おばけの色変更用
    private SpriteRenderer _GhostSprite = null;

    //お化けの透明度
    private float g_fGhostAlpha = 0;
    //透明にする判定
    private bool g_bGhostExit = false;

   [SerializeField]
    private float g_fEscapeTime = 1.0f;
    [SerializeField]
    private float g_fGhostTransSpeed = 0.01f;

    //Fadeオブジェ
    CFade fade;

    //初期化
    void Awake()
    {
        //プレイヤーを見つける
        _PlayerObj =
            GameObject.Find("Player");

        _fadeObj = GameObject.Find("Player/Canvas/Fade");

        //声を設定
        _voiceSorce = GetComponent<AudioSource>();
   
        //全部消しておく
        _GhostSprite =
            GetComponent<SpriteRenderer>();
    
        //初期化
        g_fGhostAlpha = 0;
        g_bGhostExit = false;

        fade = _fadeObj.GetComponent<CFade>();

        StartCoroutine(GhostCreate());

    }


    //画面更新
    void Update()
    {
        _GhostSprite.color =
            new Color(1, 1, 1, g_fGhostAlpha);

        //判定で消えていく
        if (g_bGhostExit)
        {
            g_fGhostAlpha -= g_fGhostTransSpeed;
            if (g_fGhostAlpha <= 0)
            {
                g_bGhostExit = false;
            }
        }

    }


    //お化け出現処理
    IEnumerator GhostCreate()
    {
        yield return null;
        //お化けを出し、カメラの前を真っ暗にする
        fade.FadeA = 1.0f;
        g_fGhostAlpha = 1.0f;

        //一定時間待つ
        yield return new WaitForSeconds(2);
        //オブジェクトにする
        transform.parent =
            _PlayerObj.transform;

        transform.localRotation = Quaternion.Euler(0, 0, 0);
        Vector3 appearPos = new Vector3(0, 0, 0);
        transform.localPosition = appearPos;
        transform.Translate(0, 0.5f, 1.0f);
        //お化けを見せながら声を出す
        fade.FadeA = 0.0f;
        _voiceSorce.PlayOneShot(_voiceSorce.clip);

        //消えるまでの時間
        yield return new WaitForSeconds(g_fEscapeTime);
        g_bGhostExit = true;

        yield return new WaitForSeconds(1.5f);
        Destroy(GameObject.Find("GhostPrefab"));
        Destroy(this.gameObject);

    }
}