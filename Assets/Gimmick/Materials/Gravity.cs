﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour
{
    private Rigidbody rigi;
	// Use this for initialization
	void Start ()
    {
        rigi = GetComponent<Rigidbody>();
        
	}
	
	void FixedUpdate()
    {
        rigi.AddForce(Vector3.up);

    }
}
