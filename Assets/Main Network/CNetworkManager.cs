﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.SceneManagement;

public class CNetworkManager : NetworkManager
{
    CPlayerFade _playerFade;
    CTime _ctime;


    private bool _connect = false;

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetButton("Fire1") || Input.GetMouseButtonDown(0)) && !_connect)
        {

            if (CTopMenu.sSelectDevice == "PC")
            {
                _connect = true;
                StartupHost();
            }
            else if (CTopMenu.sSelectDevice == "Oculus")
            {
                JoinGame();
                _connect = true;
            }
            

            if(_playerFade._oculusResultFlag || _ctime._editerResult)
            {
                StopJoin(true);
            }

        }

        //else if(Input.GetKeyDown(KeyCode.Space) && _connect)
        //{
        //    StopJoin();
        //}
    }

    //ButtonStartHostボタンを押した時に実行
    //IPポートを設定し、ホストとして接続
    public void StartupHost()
    {
        SetPort();
        //NetworkManager.singleton.StartHost();
        NetworkManager.singleton.StartServer();
    }

    //ButtonJoinGameボタンを押した時に実行
    //IPアドレスとポートを設定し、クライアントとして接続
    public void JoinGame()
    {
        SetIPAddress();
        SetPort();
        NetworkManager.singleton.StartClient();
    }

    void SetIPAddress()
    {
        //Input Fieldに記入されたIPアドレスを取得し、接続する
        //string ipAddress = GameObject.Find("InputFieldIPAddress").transform.FindChild("Text").GetComponent<Text>().text
        //NetworkManager.singleton.networkAddress = networkAddress;
    }

    //ポートの設定
    void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
    }

    public void StopJoin(bool result)
    {
        if (result)
        {
            if (CTopMenu.sSelectDevice == "PC")
            {
                _connect = false;
                NetworkManager.singleton.StopHost();
                SceneManager.LoadScene("Result");
                Destroy(gameObject);
            }
            else if (CTopMenu.sSelectDevice == "Oculus")
            {
                _connect = false;
                NetworkManager.singleton.StopClient();
                SceneManager.LoadScene("Result");
                Destroy(gameObject); 
            }
        }
        else
        {
            if (CTopMenu.sSelectDevice == "PC")
            {
                _connect = false;
                NetworkManager.singleton.StopHost();
            }
            else if (CTopMenu.sSelectDevice == "Oculus")
            {
                _connect = false;
                NetworkManager.singleton.StopClient();
            }
        }

    }
}
