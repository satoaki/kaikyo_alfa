﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

//IPアドレス:10.25.34.52

public class CDeviceCheck : /*Network*/MonoBehaviour
{
    CTopMenu _topmenu;

    //Oculus(Client側)でEditerを消す

    public GameObject GameHardManager;
    //public GameObject PCHard;
    //public GameObject OculusHard = GameObject.Find("");
    //public GameObject Player;
    public GameObject PlayerCamera;
    public GameObject PCMap;
    public GameObject OculusMap;
    public GameObject PCCamera;

    //public GameObject EditerSprite;
    //public GameObject EditerEventSystem;
    //public GameObject EditerSampleCamera;
    public GameObject Editer;
    //public GameObject EditerMap;

    private bool Check = false;

    void EditerCreate()
    {
        //EditerMap.SetActive(true);
        //PlayerCamera = GameObject.Find("Player(Clone)");
        //PlayerCamera.SetActive(false);
        //OculusHard.SetActive(false);
        OculusMap.SetActive(false);
    }

    //[ClientRpc]
    void OculusCreate()
    {
        //OculusMap.SetActive(true);

        //EditerSprite.SetActive(false);
        //Destroy(EditerSprite);
        //EditerEventSystem.SetActive(false);
        //PCCamera.SetActive(false);
        //EditerSampleCamera.SetActive(false);
        PCMap.SetActive(false);
        Editer.SetActive(false);

        //PlayerCamera = GameObject.Find("Player(Clone)");
        //PlayerCamera.SetActive(true);
        //Player.name = "Player";

        //Debug.Log((bool)PlayerCamera);
    }



    void Start()
    {
        //if (isServer)
        //{
        //    EditerCreate();
        //}
        //if (isClient)
        //{

        //    OculusCreate();

        //}
        //RpcOculusCreate();
        //ServerEditerCreate();
        //if (CTopMenu.sSelectDevice == "Oculus")
        //{
        //    //OculusCreate();
        //}
        if (CTopMenu.sSelectDevice == "PC")
        {
            EditerCreate();
        }
        //else
        //{
        //    throw new System.Exception("エラーです。");
        //}
    }
    void Update()
    {
        
        if(!Check)
        {
            GameObject.Find("Player(Clone)").name = "Player";
            //if(GameObject.Find("Player").name == "Player")
            Check = true;
        }
        if (/*isClient || */CTopMenu.sSelectDevice == "Oculus")
        {
            Debug.Log("Client");
            OculusCreate();
            //Destroy(this);
        }
        //if(isServer)
        //{
        //    PlayerCamera.SetActive(false);
        //}
        if(CTopMenu.sSelectDevice == "PC")
        {
            PlayerCamera = GameObject.Find("OculusCamera");
            if((bool)PlayerCamera)
                PlayerCamera.SetActive(false);
        }
    }

}