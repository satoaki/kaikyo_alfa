﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class CTime : NetworkBehaviour
{

    [SyncVar(hook = "OnTimeUpdate")]
    private float _localTime = 180.0f;

    public float _time;

    public bool StartTime;
    private GameObject _startTimeCollider;
    private GameObject _networkManager;

    public bool _editerResult;

    public void TimeDebug()
    {
        //g_fLimittime = 20.0f;0
        _localTime = 20.0f;
    }

    public void TimeChange()
    {
        //g_fLimittime -= Time.deltaTime;
        _localTime -= Time.deltaTime;
        //_localTime -= (1f / 60.0f);
        //_time -= (1f / 60.0f);
    }

    void OnTimeUpdate(float _changeTime)
    {
        //全クライアントへ変更結果を送信
        _localTime = _changeTime;
        _time = _localTime;
    }

    // Use this for initialization
    void Awake()
    {
        _editerResult = false;
        _time = _localTime;
        StartTime = false;
        _startTimeCollider = GameObject.Find("StartTimeCollider");
        _networkManager = GameObject.Find("NetworkManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTime)
        {
            TimeChange();
            _time = _localTime;
            OnTimeUpdate(_time);
        }
        //_time = _localTime;
        if (Input.GetKey(KeyCode.Q))
        {
            TimeDebug();
            _time = _localTime;
            OnTimeUpdate(_time);
        }
        if(_time <= 0)
        {
            //_editerResult = true;
            _networkManager.GetComponent<CNetworkManager>().StopJoin(true);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartTime = true;
            Debug.Log(StartTime + " TimerStart!");
        }
    }

}
