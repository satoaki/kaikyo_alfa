﻿using UnityEngine;
using System.Collections;

public class CClientManager : MonoBehaviour {

    [SerializeField]
    GameObject _editer;
    [SerializeField]
    GameObject _editerMap;
    //GameObject PlayerCamera;
    
	// Use this for initialization
	void Start () {
        if (CTopMenu.sSelectDevice == "Oculus")
        {
            _editer.SetActive(false);
            _editerMap.SetActive(false);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        if (CTopMenu.sSelectDevice == "Oculus")
        {
            _editer.SetActive(false);
            _editerMap.SetActive(false);
        }
    }
}
