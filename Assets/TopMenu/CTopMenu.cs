﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CTopMenu : MonoBehaviour {

    public static string sSelectDevice;

    public void EditerButton()
    {
        sSelectDevice = "PC";
        SceneManager.LoadScene("Title");
    }

    public void OculusButton()
    {
        sSelectDevice = "Oculus";
        SceneManager.LoadScene("Title");
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            EditerButton();
        }

        if (Input.GetButton("Fire1"))
        {
            OculusButton();
        }
    }

}
