﻿using UnityEngine;
using UnityEngine.UI;

public class CFade : MonoBehaviour
{
    private Image _image;
    [SerializeField]
    private float fadeA = 0.01f;
    public float FadeA
    {
        set
        {
            fadeA = value;
            _image.color = new Color(0, 0, 0, fadeA);
        }
    }

    [SerializeField, Range(0.001f, 0.1f),
        Tooltip("フェード切り替えの早さ")]
    private float fadeSpeed = 0;


    //初期化
    void Awake()
    {
        _image = GetComponent<Image>();
       
    }

    //フェードイン
    public bool Fadein(float r, float g, float b)
    {
        //色を置き換える
        _image.color = new Color(r, g, b, fadeA);

        fadeA -= fadeSpeed;

        //フェードが０になったら
        if (fadeA <= 0)
        {
            return true;
        }
        return false;
    }


    //フェードアウト
    public bool Fadeout(float r, float g, float b)
    {
        //色を置き換える()
        _image.color = new Color(r, g, b, fadeA);

        fadeA += fadeSpeed;

        //フェードが１になったら
        if (fadeA >= 1)
        {
            return true;
        }
        return false;
    }
}
