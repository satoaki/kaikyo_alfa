﻿using UnityEngine;
using System.Collections;


public class Cfishing : MonoBehaviour
{
    private SpriteRenderer sp = null;
    [SerializeField]
    private float alpha = 0;

    [SerializeField]
    private float fishing_speed = 2;

    public bool g_bmove = true;

    //初期化
    void Awake()
    {
        sp = GetComponent<SpriteRenderer>();
        g_bmove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (g_bmove)
        {
            alpha =
                Mathf.Abs(Mathf.Sin(Time.time * fishing_speed));
            sp.color =
                new Color(1, 1, 1, (alpha));
        }
        else
        {
            sp.color = new Color(1, 1, 1, 1);
        }
    }

    void Stop()
    {
        g_bmove = false;
    }

    //SpriteRenderer sp;
    //[SerializeField]
    //float nexttime, eventtime;


    //void Awake()
    //{
    // sp =   GetComponent<SpriteRenderer>();
    //    nexttime = Time.time;
    //    eventtime = 1.0f;
    //}
    //void Update() {
    //    if (Time.time > nexttime) {
    //        sp.enabled = !sp.enabled;
    //        nexttime += eventtime;
    //    }
    //}

}
