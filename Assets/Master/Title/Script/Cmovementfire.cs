﻿using UnityEngine;
using System.Collections;

public class Cmovementfire : MonoBehaviour {

    //[SerializeField]
    //private float x_speed;
    //[SerializeField]
    //private float move;

    //[SerializeField]
    //private float y_speed;

    [SerializeField]
    private float x_speed;
    [SerializeField]
    private float move;

    [SerializeField]
    private float y_speed;



    // Use this for initialization
    void Awake () {
        x_speed = 0.1f;

        y_speed = 0.05f;
    }
	
	// Update is called once per frame
	void Update () {
        move = Mathf.Sin(Time.time);
        transform.Translate(x_speed*move,0, y_speed * move);
	}
}
