﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//処理状態を名前で管理
public enum ProceName
{
    FADEIN,
    MAIN,
    FADEOUT,
}

[RequireComponent(typeof(CFade))]

public class CFadeManeger : MonoBehaviour
{

    //置き換え
    ProceName _proceName;
    CFade _fadeSc;
    string _selectDevice = CTopMenu.sSelectDevice;

    [SerializeField]
    private GameObject _fuwafuwa;

    [SerializeField]
    private ParticleSystem Particle = null;

    [SerializeField]
    private AudioSource _GenerationAudio = null;
    [SerializeField]
    private AudioSource _LoopAudio = null;
    [SerializeField]
    private AudioSource _SelectAudio = null;

    [SerializeField]
    private bool _TitleButton;

    void Awake()
    {
        _proceName = ProceName.FADEIN;
        _fadeSc = GameObject.Find("Fade").
            GetComponent<CFade>();

        _GenerationAudio = GameObject.Find("Generation").
            GetComponent<AudioSource>();
        _LoopAudio = GameObject.Find("Loop").
            GetComponent<AudioSource>();
        _SelectAudio = GameObject.Find("Select").
            GetComponent<AudioSource>();

        Particle = GameObject.Find("GhostFire").
            GetComponent<ParticleSystem>();
        Particle.Stop();

        _TitleButton = false;
    }

    public void TitleButtonCheck()
    {
        _TitleButton = true;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_proceName)
        {
            case ProceName.FADEIN:
                if (_fadeSc.Fadein(0, 0, 0))
                {
                    _proceName = ProceName.MAIN;
                    Particle.Play();

                    //出現音
                    _GenerationAudio.PlayOneShot(_GenerationAudio.clip);

                    _LoopAudio.loop = true;
                    _LoopAudio.Play();
                }
                break;
            case ProceName.MAIN:
                if (Input.GetKeyDown("space") || Input.GetButton("ButtonBPC") || _TitleButton || Input.GetMouseButtonDown(0))
                {
                    _SelectAudio.Play();
                    _proceName = 
                        ProceName.FADEOUT;
                }

                break;

            case ProceName.FADEOUT:
                if (_fadeSc.Fadeout(0, 0, 0))
                {
                    Debug.Log(CTopMenu.sSelectDevice);
                    SceneManager.LoadScene("Tutorial");
                    //if(_selectDevice == "PC")
                    //{
                    //    SceneManager.LoadScene("Tutorial_Ghostside");
                    //}
                    //if (_selectDevice == "Oculus")
                    //{
                    //    SceneManager.LoadScene("Tutorial_Reibaishiside");
                    //}

                }
                   
                break;
        }
    }
}
