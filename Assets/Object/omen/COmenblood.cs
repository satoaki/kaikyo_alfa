﻿using UnityEngine;
using System.Collections;

public class COmenblood : MonoBehaviour {

    [SerializeField]
    GameObject particleobj1;
    ParticleSystem part1;

    [SerializeField]
    GameObject particleobj2;
    ParticleSystem part2;

    void Start ()
    {
        part1 = particleobj1.GetComponent<ParticleSystem>();
        part2 = particleobj2.GetComponent<ParticleSystem>();
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            particleobj1.SendMessage("PStart");
            particleobj2.SendMessage("PStart");
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            particleobj1.SendMessage("PEnd");
            particleobj2.SendMessage("PEnd");
        }
    }
}
