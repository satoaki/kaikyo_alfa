﻿using UnityEngine;
using System.Collections;

public class FusumaMove : MonoBehaviour
{
    [SerializeField]
    private Vector3 openPos;
    private Vector3 closePos;

    [SerializeField]
    protected bool g_bdirection = false;
    //移動
    protected bool g_bmove = false;

    //開く速さ
    [SerializeField]
    private float g_fmovespeed = 0.02f;

    void Awake()
    {
        g_bdirection = true;
        g_bmove = false;
        openPos = transform.localPosition + openPos;
        closePos = transform.localPosition;
    }

    void Update()
    {
        FusumaUpdate();
    }

    public virtual void FusumaUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    g_bmove = true;
        //}

        if (g_bmove)
        {
            if (g_bdirection)
            {
                Move(openPos, g_fmovespeed, g_bdirection, g_bmove);
                if (transform.localPosition == openPos)
                {
                    g_bmove = false;
                    g_bdirection = false;
                }
            }
            else
            {
                Move(closePos, g_fmovespeed, g_bdirection, g_bmove);
                if (transform.localPosition.x == closePos.x)
                {
                    g_bmove = false;
                    g_bdirection = true;
                }
            }
        }
    }

    public virtual void Move(Vector3 setpos, float speed,
        bool direction, bool movebool)
    {
        transform.localPosition = Vector3.MoveTowards(
            transform.localPosition,
            setpos,
            speed);
    }

    public virtual void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            if (!g_bdirection) return;
            g_bmove = true;
        }
    }

    public virtual void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (g_bmove) return;
            if (g_bdirection) return;
            g_bmove = true;
        }
    }


    public virtual void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            if(g_bdirection)g_bmove = true;
        }
    }

}
