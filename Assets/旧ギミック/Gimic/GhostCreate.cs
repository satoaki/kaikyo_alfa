﻿using UnityEngine;

public class GhostCreate : MonoBehaviour
{
    [SerializeField]
    private GameObject Prefab; //表示する画像

    private GameObject clone; 
    GameObject _CameraObj;
    bool ghost_on = false;

    private void Awake()
    {

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.G) && !ghost_on)
        {

            ghost_on = true;
            clone = Instantiate(Prefab, transform.position, transform.rotation) as GameObject;
            clone.transform.name = "Gorst";
            _CameraObj = GameObject.Find("Main Camera");
            clone.transform.parent = _CameraObj.transform;
          
        }
     
    }
}

