﻿using UnityEngine;
using System.Collections;

public class TextureTest : MonoBehaviour
{

    SpriteRenderer spriterenderer;

    private float Alpha = 0;

    // Use this for initialization
    void Awake()
    {
        spriterenderer =
            GetComponent<SpriteRenderer>();
        spriterenderer.color = new Color(1, 1, 1, 0);
        Alpha = 0;
    }

    // Update is called once per frame
    void Update()
    {
        spriterenderer.color = 
            new Color(1, 1, 1, Alpha);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Alpha = 1.0f;
        }

        if(Alpha >= 0)
        {
            Alpha -= 0.01f;
        }
    }
}
