﻿using UnityEngine;
using System.Collections;

public class CSeCreater : MonoBehaviour
{
    [SerializeField]
    private GameObject _SEobj;

    public static bool g_bRing;
    // Use this for initialization
    void Awake()
    {
        g_bRing = false;
    }

    // Update is called once per frame
    void Update()
    {
        //左クリックをしたら
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (g_bRing == false)
            {
                g_bRing = true;
                Instantiate(_SEobj);
            }
        }
    }


    void Audiostop()
    {
        g_bRing = false;
    }

}
