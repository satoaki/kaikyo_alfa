﻿using UnityEngine;
using System.Collections;

public class CSE : MonoBehaviour
{
    private AudioSource _audioSource;
    private float g_fLen;

    public bool g_iAudionumber;

    void Awake()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
        g_iAudionumber = false;
    }

    void Update()
    {
        SoundPlay();
    }

    void SoundPlay()
    {
        if (!g_iAudionumber)
        {
            g_iAudionumber = true;
            _audioSource.clip = Resources.Load<AudioClip>("Sound/GrassBreak_2s");
            g_fLen = _audioSource.clip.length;
            _audioSource.PlayOneShot(_audioSource.clip, 1);
        }
        g_fLen -= Time.deltaTime;

        //音が鳴り終わったら自身をを抹消する
        if (g_fLen < 0.0f)
        {
            Destroy(gameObject);
        }

    }
}
