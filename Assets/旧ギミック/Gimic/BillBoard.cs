﻿using UnityEngine;


public class BillBoard : MonoBehaviour
{

    public Transform targetToFace;
    public bool isAutoFace = true;


    private Renderer material;
    public float alpha = 1.0f;

    void Start()
    {
        if (targetToFace == null && isAutoFace)
        {
            var mainCameraObject = GameObject.FindGameObjectWithTag("MainCamera");
            targetToFace = mainCameraObject.transform;
            material = GetComponent<Renderer>();
            material.material.color = new Color(1.0f, 1.0f, 1.0f, alpha);
        }
    }


    void Update()
    {
        alpha -= 0.01f;
        if (alpha <= 0.0f) isAutoFace = false;
        
        if(!isAutoFace)
        {
            Destroy(this.gameObject);
        }
    }
}
