﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Test : NetworkBehaviour
{

    [SerializeField]
    GameObject cube;

    // Use this for initialization
    void Start()
    {
        if (isServer)
        {
            Debug.Log("isServer: " + isServer);
            Debug.Log("NetworkServer.active" + NetworkServer.active);
            var _cube = Instantiate(cube);
            NetworkServer.Spawn(_cube);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("isServer: " + isServer);
        Debug.Log("NetworkServer.active" + NetworkServer.active);
    }
}
