﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Test01 : NetworkBehaviour {

    [SyncVar]
    float a = 0;

    [Command]
    void CmdChange(float value)
    {
        a = value;
    }

	
	// Update is called once per frame
	void Update () {

        if(!isServer)
        {
            CmdChange(0.01f);
        }

        if(isServer)
        {
            
            Debug.Log(a);
            transform.Translate(a, 0, 0);
        }
    }
}
