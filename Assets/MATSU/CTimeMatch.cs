﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CTimeMatch : NetworkBehaviour
{

    //private CTimekeeper _cTimekeeperSc = null;
    private float _count;
    //[SerializeField]
    //private GameObject Candle;

    private GameObject _gameController;

    //分
    private int minute = 0;
    //秒　10の位
    private int second10 = 0;
    //秒　1の位
    private int second1 = 0;

    private List<Sprite> number;

    //分
    private Image _minImage = null;
    //秒　10の位
    private Image _secImage10 = null;
    //秒　1の位
    private Image _secImage1 = null;

    [SerializeField]
    bool _debugTimeStop = false;

    //初期化
    void Awake()
    {
        //if (!_debugTimeStop)
        //_cTimekeeperSc = GameObject.Find(Candle).
        //    GetComponent<CTimekeeper>();
        //_cTimekeeperSc = Candle.GetComponent<CTimekeeper>();

        _gameController = GameObject.Find("GameController");
        _count = _gameController.GetComponent<CTime>()._time;

        minute = 0;
        second10 = 0;
        second1 = 0;

        _minImage = GameObject.Find("Min").
            GetComponent<Image>();
        _secImage10 = GameObject.Find("Sec10").
            GetComponent<Image>();
        _secImage1 = GameObject.Find("Sec1").
            GetComponent<Image>();

        number = new List<Sprite>();
        number.AddRange(
            Resources.LoadAll<Sprite>("Texture/UI/UI_number")
        );
    }

    // Update is called once per frame
    void Update()
    {
        _count = _gameController.GetComponent<CTime>()._time;
        //_cTimekeeperSc = Candle.GetComponent<CTimekeeper>();
        TimeMatch(_debugTimeStop, /*_cTimekeeperSc.LimitTime*/_count);
    }

    //[Command]
    void TimeMatch(bool debug_stop, float time)
    {
        //時間を読み込み＆読みづらいので書き換え
        if (!debug_stop)
        {

            Debug.Log(time);

            minute = ((int)time) / 60;
            second10 = (((int)time) % 60) / 10;
            second1 = (((int)time) % 60) % 10;

            //Debug.Log(CTimekeeper.g_fLimittime);

            //minute = ((int)CTimekeeper.g_fLimittime) / 60;
            //second10 = (((int)CTimekeeper.g_fLimittime) % 60) / 10;
            //second1 = (((int)CTimekeeper.g_fLimittime) % 60) % 10;

            //画像を切り替える
            _minImage.sprite = number[minute];
            _secImage10.sprite = number[second10];
            _secImage1.sprite = number[second1];

        }
    }
}
