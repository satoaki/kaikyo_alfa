﻿using UnityEngine;
using System.Collections;
//using UnityEngine.Networking;

public class CMainCameraMove : MonoBehaviour {

    [SerializeField, Range(3, 5), Tooltip("ズームスピード")]
    private float _cameraZoomSpeed = 3.0f;

    [SerializeField, Range(0.1f, 0.3f), Tooltip("カメラスピード")]
    private float _cameraMoveSpeed = 0.1f;

    //カメラの移動の最大、最小決め
    [SerializeField]
    private float _camMoveMinX;
    [SerializeField]
    private float _camMoveMaxX;
    [SerializeField]
    private float _camMoveMinZ;
    [SerializeField]
    private float _camMoveMaxZ;
    //[SerializeField]
    //private float _camZoomMinY;
    //[SerializeField]
    //private float _camZoomMaxY;
    [SerializeField]
    private Camera _camera = null;
    //マウスのポジション
    
    private Vector3 _mousePos;
    private Vector3 _oldMousePos;

    void Start()
    {
        _mousePos = Input.mousePosition;
        _camera = GameObject.Find("EditerCamera").GetComponent<Camera>();
	}
	
	void Update ()
    {
        Move();
	}

    
    void Move()
    {
        float camZoom = Input.GetAxis("Mouse ScrollWheel");
        //マウスのズーム関連
        transform.Translate(0, -(_cameraZoomSpeed * camZoom), 0, Space.World);

        //マウスホイール押し込み操作
        if(Input.GetMouseButton(2))
        {
            _oldMousePos = _mousePos;
            _mousePos = Input.mousePosition;
            transform.Translate((_mousePos.x - _oldMousePos.x) * _cameraMoveSpeed, 0, (_mousePos.y - _oldMousePos.y) * _cameraMoveSpeed, Space.World);
        }
        else 
        {
            _mousePos = Input.mousePosition;            
        }

        //以下カメラの限界値処理
        if(transform.localPosition.x < _camMoveMinX)
        {
            transform.localPosition = new Vector3(_camMoveMinX, transform.localPosition.y, transform.localPosition.z);
        }
        else if(transform.localPosition.x > _camMoveMaxX)
        {
            transform.localPosition = new Vector3(_camMoveMaxX, transform.localPosition.y, transform.localPosition.z);
        }

        if(transform.localPosition.z < _camMoveMinZ)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, _camMoveMinZ);
        }
        else if(transform.localPosition.z > _camMoveMaxZ)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, _camMoveMaxZ);
        }

        _camera.orthographicSize -= camZoom * 1.5f;
        if(_camera.orthographicSize < 3.0f)
        {
            _camera.orthographicSize = 3.0f;
        }
        else if(_camera.orthographicSize > 15.0f)
        {
            _camera.orthographicSize = 15.0f;
        }

        //if(transform.localPosition.y < _camZoomMinY)
        //{
        //    transform.localPosition = new Vector3(transform.localPosition.x, _camZoomMinY, transform.localPosition.z);
        //}
        //else if(transform.localPosition.y > _camZoomMaxY)
        //{
        //    transform.localPosition = new Vector3(transform.localPosition.x, _camZoomMaxY, transform.localPosition.z);
        //}
    }
}
