﻿using UnityEngine;
using System.Collections;

public class cButton_M : MonoBehaviour
{

    //選択後のパネル
    private GameObject setPanelObj = null;

    //初期化
    void Awake()
    {
        setPanelObj = GameObject.Find("SetPanel");
        setPanelObj.SetActive(false);
    }

    //画面更新
    void Update()
    {
    }

    //クリックされたらの処理
    public void isClick()
    {
        //パネルを付ける
        setPanelObj.SetActive(true);
        setPanelObj.transform.position =
               gameObject.transform.position;

    }
}
