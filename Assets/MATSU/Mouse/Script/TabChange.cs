﻿using UnityEngine;
using System.Collections;

public class TabChange : MonoBehaviour {

    [SerializeField]
    private GameObject _Panel;

    [SerializeField]
    private GameObject[] _page;
    [SerializeField]
    private GameObject[] _type;
    [SerializeField]
    private GameObject[] _text;
    [SerializeField]
    private GameObject _SEButton;

    private static bool _flug = false;

    public void ClickTab()
    {
        _Panel.transform.SetAsLastSibling();

        _page[0].SetActive(false);
        _page[1].SetActive(true);
        _type[0].SetActive(true);
        _type[1].SetActive(false);
        _text[0].SetActive(true);
        _text[1].SetActive(false);
        _flug = !_flug;
        _SEButton.SetActive(_flug);
        
    }
}
