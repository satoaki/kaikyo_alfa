﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class CMakePoint : NetworkBehaviour
{

    private Ray _ray;
    private RaycastHit _hit;

    [SerializeField]
    private GameObject _setGimick;
    public GameObject Gimick {
        get { return _setGimick; }
        set { _setGimick = value; }
    }

    private int _count = 0;
    
    void Start()
    {

    }

    void Update()
    {
        if (_setGimick == null) return;
        if (Input.GetMouseButtonDown(0))
        {
            _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray, out _hit, 100f))
            {
                Debug.Log("UpdateClick");
                CreateGimick();
            }
        }
        
    }

    //[Command]
    void CreateGimick()
    {
        Debug.Log("Create");
        _count++;
        //var _gimick = Instantiate(_setGimick, );
        //Instantiate(setGimick, new Vector3(hit.point.x, hit.point.y + 1.0f, hit.point.z), setGimick.transform.localRotation);
        var _setGimickClone = Instantiate(_setGimick, new Vector3(_hit.point.x, 0.02f, _hit.point.z), _setGimick.transform.localRotation) as GameObject;
        NetworkServer.Spawn(_setGimickClone);
        Debug.Log(_setGimickClone.name);
    }

}
