﻿using UnityEngine;
using System.Collections;

public class cResultItem : MonoBehaviour
{
    private float rotatespeed = 0f;
    private float scaleItems = 1f;

    //背景一枚絵
    private SpriteRenderer texRenderer = null;
    //フェード
    private GameObject fadeObj = null;
    private SpriteRenderer fadeRenderer = null;

    //色を出すための変数
    private float textrans = 0;
    private float fadetrans = 0;

    private GameObject GhostObj = null;
    //初期化
    void Awake()
    {
        scaleItems = 
            transform.localScale.x;

        texRenderer =
            GameObject.Find("WinnerTex").GetComponent<SpriteRenderer>();

        fadeObj = GameObject.Find("winner_fade");
        fadeRenderer = 
            GameObject.Find("winner_fade").GetComponent<SpriteRenderer>();

        GhostObj = GameObject.Find("Ghost");
    }

    //画面更新
    void Update()
    {
        //回転させる
        rotatespeed += 0.02f;
        transform.Rotate(new Vector3(0, 0, rotatespeed));

        //回転スピードが一定以上で
        if(rotatespeed >= 5)
        {
            if(scaleItems >= 0)
            {
                //真ん中に集結
                scaleItems -= 0.01f;
                transform.localScale =
                    new Vector3(scaleItems, scaleItems, 1f);
            }
            else
            {
                transform.localScale =
                    new Vector3(0, 0, 0);
                fadetrans += 0.02f;
                fadeRenderer.color = 
                    new Color(0, 0, 0, fadetrans);
                if(fadetrans >= 1)
                {
                    Destroy(GhostObj);
                    fadeObj.SetActive(false);
                    textrans += 0.02f;
                    texRenderer.color =
                        new Color(textrans, textrans, textrans);
                }
            }
        }
    }
}
