﻿using UnityEngine;
using UnityEngine.UI;

public class CUIMove : MonoBehaviour
{
    [SerializeField]
    private GameObject _selectObj = null;
    [SerializeField]
    private GameObject _setPanel = null;
    [SerializeField]
    private GameObject[] _targetObj = null;
    public GameObject _createObj = null;

    public GameObject _subFrameObj = null;

    private int g_iselect = 0;

    //クリエイト側
    private GameObject _FakeObject = null;
   
    private bool g_bselectlr = true;
    private bool g_bselectud = true;

    private bool g_bButton = false;

    private Vector3 moveScale = Vector3.zero;

    //初期化
    void Awake()
    {
        g_iselect = 0;

        _subFrameObj = GameObject.Find("subFrame");

        _selectObj =
            GameObject.Find("SelectPanel");
        _setPanel =
            GameObject.Find("SetPanel");
        moveScale = new Vector3(1.2f, 1.2f, 1);
    }

    //毎フレームごとの更新
    void Update()
    {
        //選択してるオブジェクトに場所を合わせる
        _selectObj.transform.position =
            _targetObj[g_iselect].transform.position;

        _setPanel.transform.position =
           _targetObj[g_iselect].transform.position;


        moveSelect();
        //タグによって捜査範囲を変更
        Select();
    }

    //カーソルの動き
    private void moveSelect()
    {
        _selectObj.transform.localScale =
            moveScale;

        _setPanel.transform.localScale =
        moveScale;

        moveScale.x -= 0.005f;
        moveScale.y -= 0.005f;
        if (moveScale.x <= 1.0f)
        {
            moveScale = new Vector3(1.2f, 1.2f, 1);
        }
    }

    //選択場所の移動
    void Select()
    {
        float selectud = Input.GetAxis("KeyArrowUD");
        if (selectud == -1 && g_bselectud)
        {
            g_iselect++;
            g_bselectud = false;
        }
        else if (selectud == 1 && g_bselectud)
        {
            g_iselect--;
            g_bselectud = false;
        }
        else if (selectud == 0 && !g_bselectud)
        {
            g_bselectud = true;
        }

        float selectlr = Input.GetAxis("KeyArrowLR");
        if (selectlr == -1 && g_bselectlr)
        {
            g_iselect += 3;
            g_bselectlr = false;
        }
        else if (selectlr == 1 && g_bselectlr)
        {
            g_iselect -= 3;
            g_bselectlr = false;
        }
        else if (selectlr == 0 && !g_bselectlr)
        {
            g_bselectlr = true;
        }


        //キーで選択部分を移動 上下
        if (Input.GetKeyDown(KeyCode.B)) { g_iselect--; }
        if (Input.GetKeyDown(KeyCode.M)) { g_iselect++; }
        if (Input.GetKeyDown(KeyCode.J)) { g_iselect--; }
        if (Input.GetKeyDown(KeyCode.N)) { g_iselect++; }

        //最大まで行ったら戻る
        if (g_iselect == -1) { g_iselect = 5; }
        if(g_iselect == -2) { g_iselect = 4; }
        if(g_iselect == -3) { g_iselect = 3; }
        if (g_iselect == 6) { g_iselect = 0; }
        if(g_iselect == 7) { g_iselect = 1; }
        if(g_iselect == 8) { g_iselect = 2; }
        //選択
        if (Input.GetKeyDown(KeyCode.K))
        {
            _targetObj[g_iselect].
                SendMessage("SettingObject");

            _subFrameObj.SetActive(false);

            //サンプル表示
            _FakeObject =
                Instantiate(_createObj,
                new Vector3(0, 1000, 3),
                _createObj.transform.rotation) as GameObject;

            _FakeObject.name = "FakeObject";

            //タグを変える
            tag = "Create";
        }
        
        //選択(ゲームパッド)
        if (Input.GetButton("ButtonBPC") && !g_bButton)
        {
            g_bButton = true;
            _targetObj[g_iselect].
                SendMessage("SettingObject");

            //サンプル表示
            _FakeObject =
                Instantiate(_createObj,
                new Vector3(0, 1000, 3),
                _createObj.transform.rotation) as GameObject;

            _subFrameObj.SetActive(false);

            _FakeObject.name = "FakeObject";

            //タグを変える
            tag = "Create";
        }
        else if(!Input.GetButton("ButtonBPC") && g_bButton)
        {
            g_bButton = false;
        }
    }
}
