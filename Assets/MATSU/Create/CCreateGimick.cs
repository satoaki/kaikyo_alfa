﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class CCreateGimick : NetworkBehaviour
{

    private Renderer _PointColor = null;
    private bool g_bCreate1 = false;
    public bool Create1
    {
        set { g_bCreate1 = value; }
    }

    bool g_bButtonDown = true;
    

    public GameObject _SetGimick;
    // Use this for initialization
    void Awake()
    {
        _PointColor =
            GetComponent<Renderer>();
    }
    

    // Update is called once per frame
    void Update()
    {

        if(Input.GetButton("ButtonAPC"))
        {
            g_bButtonDown = true;
            Create1 = false;
            enabled = false;
        }
    
        if (g_bCreate1)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                //Debug.Log("Push K");
                //StartCoroutine("Create");
                CmdCreate(_SetGimick);
            }

            if(Input.GetButton("ButtonBPC") && !g_bButtonDown)
            {
                StartCoroutine("Create");
                g_bButtonDown = true;
            }
            else if(!Input.GetButton("ButtonBPC") && g_bButtonDown)
            {
                g_bButtonDown = false;
            }
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Floor")
        {
            _PointColor.material.color =
                Color.green;
            g_bCreate1 = true;
        }
        else
        {
            _PointColor.material.color =
                Color.red;
            g_bCreate1 = false;
        }
    }

    [Command]
    void CmdCreate(GameObject gameObject)
    {
        var _gimick = Instantiate(gameObject, transform.position, gameObject.transform.rotation
            ) as GameObject;
        NetworkServer.Spawn(_gimick);

        _gimick.name = gameObject.name;
    }

    IEnumerator Create()
    {
        //GameObject _cloneObj;

        //_cloneObj = Instantiate(_SetGimick,
        //    transform.position,
        //    _SetGimick.transform.rotation) as GameObject;

        var _gimick = Instantiate(_SetGimick, transform.position, _SetGimick.transform.rotation
            )as GameObject;
        NetworkServer.Spawn(_gimick);

        _gimick.name = _SetGimick.name;
        

        yield return new WaitForSeconds(0.2f);
    }
}
