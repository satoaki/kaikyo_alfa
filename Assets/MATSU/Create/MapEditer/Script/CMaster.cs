﻿using UnityEngine;

public class CMaster : MonoBehaviour
{
    //作るマップの個数
    public int g_iMapCountX = 0;
    public int g_iMapCountZ = 0;

    //選択している配列番号
    public int g_iSetNumber = 0;

    //配置しているオブジェクト
    public int g_iObjectCount = 0;
}
