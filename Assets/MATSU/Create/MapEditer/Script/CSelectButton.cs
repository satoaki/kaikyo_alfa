﻿using UnityEngine;
using System.Collections;

public class CSelectButton : MonoBehaviour
{

    //視覚系オブジェクト
    private GameObject _visionObj = null;
    //聴覚系オブジェクト
    private GameObject _hearingObj = null;

    //マップのまとめオブジェクト
    private GameObject _mapObj = null;
    //オブジェクトのまとめオブジェクト
    private GameObject _objectObj = null;

    //生成するスクリプトを取得
    private CObjectCreate _CObjectCreate = null;
    //アイコンのスクリプト
    private CSelectIcon _CSelectIcon = null;
    //マスター
    private CMaster _CMaster = null;

    private bool bmode = true;

    //初期化
    void Awake()
    {
        //オブジェクトを当てはめる
        _visionObj = GameObject.Find("VisionObject");
        _hearingObj = GameObject.Find("HearingObject");

        _mapObj = GameObject.Find("MapCreater");
        _objectObj = GameObject.Find("ObjectCreater");

        //表示を消しておく
        _visionObj.SetActive(false);
        _hearingObj.SetActive(false);

        //生成するスクリプトのついたオブジェクトを探す
        _CObjectCreate =
            GameObject.Find("ObjectCreater")
            .GetComponent<CObjectCreate>();
        //アイコンオブジェクトを探す
        _CSelectIcon = GameObject.Find("SelectIcon").
            GetComponent<CSelectIcon>();
        //マスターを探す
        _CMaster = GameObject.Find("GameMaster").
            GetComponent<CMaster>();
    }

    //視覚系か聴覚系か調べて対応したものを表示
    public void SelectType(string typeName)
    {
        //上部に視覚系ギミックを表示
        if (typeName == "Vision")
        {
            _visionObj.SetActive(true);
            _hearingObj.SetActive(false);
        }
        //上部に聴覚系ギミックを表示
        if (typeName == "Hearing")
        {
            _hearingObj.SetActive(true);
            _visionObj.SetActive(false);
        }
    }

    //オブジェクトを選択するボタン
    public void SelectButton(string objectName)
    {
        if (objectName == "Sample")
        {
            //生成する対象を変更
            _CMaster.g_iSetNumber = 0;
            Read();
        }

        if (objectName == "kokeshi")
        {
            //生成する対象を変更
            _CMaster.g_iSetNumber = 1;
            Read();
        }

        if (objectName == "vase")
        {
            //生成する対象を変更
            _CMaster.g_iSetNumber = 2;
            Read();
        }
    }

    //削除ボタン
    public void DeleteButton(string deleteType)
    {
        //全削除
        if (deleteType == "All")
        {
            foreach (Transform gimic in _objectObj.transform)
            {
                Destroy(gimic.gameObject);
            }

            //カウントも初期化
            _CMaster.g_iObjectCount = 0;

            foreach (Transform map in _mapObj.transform)
            {
                if (map.tag == "End")
                {
                    map.tag = "Map";
                }
            }
        }

        //個々に削除
        if (deleteType == "One")
        {

        }
    }

    //ビューモード切り替え
    public void ModeChange()
    {
        if (bmode)
        {
            _mapObj.SetActive(false);
            bmode = false;
        }
        else
        {
            _mapObj.SetActive(true);
            bmode = true;
        }
    }

    //読み込ませる
    void Read()
    {
        _CSelectIcon.SendMessage("ReadMaster");
        _CObjectCreate.SendMessage("ReadMaster");
    }
}
