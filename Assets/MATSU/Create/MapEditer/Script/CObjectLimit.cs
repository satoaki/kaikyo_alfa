﻿using UnityEngine;
using UnityEngine.UI;

public class CObjectLimit : MonoBehaviour
{

    [SerializeField]
    private int g_iObjCount = 0;
    [SerializeField]
    private int g_iMaxCount = 10;

    //書き込むテキストを指定
    private Text _CountText = null;
    //オブジェクト配置
    private GameObject _ObjectCreate;
    //マスター
    private CMaster _CMaster = null;

    //初期化
    void Awake()
    {
        g_iObjCount = 0;
        _CountText = GetComponent<Text>();
        _ObjectCreate =
            GameObject.Find("ObjectCreater");
        _CMaster = GameObject.Find("GameMaster").
            GetComponent<CMaster>();
    }

    //画面更新
    void Update()
    {
        //オブジェクト数を読み込む
        g_iObjCount = _CMaster.g_iObjectCount;

        _CountText.text =
            "配置コスト \n"
            + g_iObjCount + 
            "/"
            + g_iMaxCount ;

        if(g_iObjCount >= g_iMaxCount)
        {
            _CountText.color = Color.red;
            _ObjectCreate.tag = "Wait";
        }
        else
        {
            _CountText.color = Color.blue;
            _ObjectCreate.tag = "Create";
        }
    }
}
