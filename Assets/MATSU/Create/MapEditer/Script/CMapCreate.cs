﻿using UnityEngine;
using System.Collections;

public class CMapCreate : MonoBehaviour {
    [SerializeField]
    private GameObject _floorObj = null;
    private GameObject[,] _mapsObj = null;

    private int g_iCountX = 0;
    private int g_iCountZ = 0;

    //マスター
    CMaster _CMaster;

    //初期化
    void Awake()
    {
        //マスターを探す
        _CMaster = GameObject.Find("GameMaster").
            GetComponent<CMaster>();

        //マスターから情報を読み込む
        g_iCountX = _CMaster.g_iMapCountX;
        g_iCountZ = _CMaster.g_iMapCountZ;
    }

    void Start()
    {
        //カウント分オブジェクトをつくる準備
        _mapsObj =
            new GameObject[g_iCountX, g_iCountZ];

        for (int x = 0; x < g_iCountX; ++x)
        {
            for (int z = 0; z < g_iCountZ; ++z)
            {

                //位置を移動
                float fPosX = g_iCountX / 2.0f;
                float fPosZ = g_iCountZ / 2.0f;

                //個数分生成
                _mapsObj[x, z] = Instantiate(_floorObj,
                    new Vector3(-fPosX + x,
                        0,
                        -fPosZ + z),
                    _floorObj.transform.rotation) as GameObject;

                //オブジェクトに名前を付ける
                _mapsObj[x, z].transform.name =
                    "Map[" + x + ", " + z + "]";

                _mapsObj[x, z].transform.parent =
                    gameObject.transform;
            }
        }
    }
}
