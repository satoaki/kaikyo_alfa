﻿using UnityEngine;

public class CMap : MonoBehaviour
{

    //オブジェクトが置けるかどうかの判定
    private bool g_bSetting = false;
    //色を取得するためのマテリアル
    private Renderer _material;

    //マスター
    private CMaster _CMaster;

    //初期化
    void Awake()
    {
        g_bSetting = false;
        _material =
            GetComponent<Renderer>();
    }

    //画面更新
    void Update()
    {
        if (!g_bSetting)
        {
            //基本色
            _material.material.color =
                new Color(1, 1, 1, 0.2f);

            if (tag == "Hit")
            {
                //選択してるタグに切り替え
                tag = "Map";
                _material.material.color =
                    new Color(1.0f, 0, 0, 0.2f);
            }
        }
        if (tag == "End")
        {
            g_bSetting = true;
            _material.material.color =
                new Color(0, 0, 1.0f, 0.2f);
        }
        else
        {
            g_bSetting = false;
        }
    }
}
