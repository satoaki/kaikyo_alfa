﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSelectIcon : MonoBehaviour
{
    public Image _image;


    [SerializeField]
    public Sprite _noneSprite;

    private Sprite _targetSprite;
    [SerializeField]
    public Sprite[] _iconSprite;

    private CMaster _CMaster;

    //初期化
    void Awake()
    {
        _image = GetComponent<Image>();
        _CMaster = GameObject.Find("GameMaster").
            GetComponent<CMaster>();

        //Noneに初期化
        _targetSprite = _noneSprite;
        
    }

    //アイコン変更
    void ReadMaster()
    {
        //選択オブジェクトを変更
        _targetSprite =
            _iconSprite[_CMaster.g_iSetNumber];

        //イメージを変える
        _image.sprite = _targetSprite;
    }
}
