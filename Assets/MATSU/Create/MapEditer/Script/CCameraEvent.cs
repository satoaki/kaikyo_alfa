﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CCameraEvent : MonoBehaviour
{
    //注視点
    private GameObject focusObj = null;

    //マウスの位置を保存する
    private Vector3 oldPos;

    //
    void setUpFocusObject(string name)
    {
        GameObject obj = focusObj = new GameObject(name);
        obj.transform.position = Vector3.zero;
    }

    //初期化
    void Awake()
    {
        if (focusObj == null)
            setUpFocusObject("focus");

        Transform trans = transform;
        transform.parent = focusObj.transform;

        trans.LookAt(focusObj.transform.position);
    }

    void Update()
    {
        CameraZoom();

        Vector3 diff = Input.mousePosition - oldPos;


        //ホイールを押しながら移動でカメラ移動
        if (Input.GetMouseButtonDown(2))
        {
            oldPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(2))
        {
            CameraTranslate(-diff* 0.1f);
        }

        SceneMove();
    }

    void CameraZoom()
    {
        float delta =
            Input.GetAxisRaw("Mouse ScrollWheel");

        if (delta != 0.0f)
            transform.Translate(0, 0, delta * 10);
    }

    void CameraTranslate(Vector3 vec)
    {
        Transform focusTrans = focusObj.transform;

        //カメラのローカル座標軸を元に注視点を移動
        focusTrans.Translate
            (
            (transform.localPosition.y * -vec.x) * 0.002f,
            focusTrans.localPosition.y,
            (transform.localPosition.y * -vec.y) * 0.002f
            );
    }

    //シーン切り替え
    void SceneMove()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("Result");
        }
    }
}
