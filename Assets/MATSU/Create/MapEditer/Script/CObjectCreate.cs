﻿using UnityEngine;
using System.Collections;

public class CObjectCreate : MonoBehaviour
{

    //作成したギミックを当てはめる
    [SerializeField]
    private GameObject[] _gimicObj = null;
    //生成するオブジェクト
    private GameObject _targetObj = null;

    //マスター
    private CMaster _CMaster;

    //初期化
    void Awake()
    {
        //マスターを探す
        _CMaster = GameObject.Find("GameMaster").
            GetComponent<CMaster>();
    }

    //画面更新
    void Update()
    {
        //カメラからRay取ってくる
        var ray =
            Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit =
            new RaycastHit();

        if (Physics.Raycast(ray, out hit))
        {
            //名前が長いので以降は_objとする
            GameObject _obj =
                        hit.collider.gameObject;

            if (_obj.tag == "Map")
            {
                _obj.tag = "Hit";
            }

            if (_obj.tag == "Hit")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if(_targetObj != null)
                    {
                        //連続で置けないようにタグがあっていたら
                        if (gameObject.tag == "Create")
                        {
                            _obj.tag = "End";

                            //コルーチンの中身は以下にある
                            StartCoroutine(CreateObject(_obj.transform.position.x,
                                _targetObj.transform.position.y,
                                _obj.transform.position.z,
                                _targetObj,
                                _targetObj.transform.rotation));

                            //配置数を変える
                            ++_CMaster.g_iObjectCount;
                        }
                    }
                }
            }
        }
    }

    //マスターの情報を読む
    void ReadMaster()
    {
        _targetObj = _gimicObj[_CMaster.g_iSetNumber];
    }

    //オブジェクト生成のコルーチン
    IEnumerator CreateObject(float posX, float posY, float posZ,
        GameObject targetObj,
        Quaternion targetQuaternion)
    {
        //タグを変更し、一定時間生成できなくする
        gameObject.tag = "Wait";

        GameObject _SetObj;

        //オブジェクトのクローンを生成
        _SetObj = Instantiate(targetObj,
            new Vector3(posX,
                posY,
                posZ),
                targetQuaternion) as GameObject;

        //子オブジェクトとする
        _SetObj.transform.parent =
            gameObject.transform;

        //置くまで少し待つ
        yield return new WaitForSeconds(0.5f);

        //タグを切り替え、再度生み出せる状態にする
        gameObject.tag = "Create";
    }
}
