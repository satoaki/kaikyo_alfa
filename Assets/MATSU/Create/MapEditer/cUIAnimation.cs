﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class cUIAnimation : MonoBehaviour
{
    //付いているイメージ
    Image _image = null;

    //インスペクターで何枚の画像なのか又どの画像なのか
    [SerializeField]
    public Sprite[] kao_image = null;

    //カウントで画像を切り替えるので
    private int _time = 0;
    [SerializeField]
    private int changetime = 0;
    private int _count = 0;

    //初期化
    void Awake()
    {
        //イメージを見つけてくる
        _image = GetComponent<Image>();

        //最初の画像に設定しておく
        _count = 0;
    }

    //画面更新
    void Update()
    {
        ++_time;
        _image.sprite = kao_image[_count];

        //一定フレームで切り替え
        if (_time >= changetime)
        {
            if(_count < kao_image.Length -1)
            {
                _count++;
            }
            else
            {
                _count = 0;
            }
            _time = 0;
        }
    }
}
