﻿using UnityEngine;
using System.Collections;

public class CPointRange : MonoBehaviour
{
    private SpriteRenderer _sp;
    public bool g_bPermissonRange = false;
    
    //初期化
    void Awake()
    {
        g_bPermissonRange = false;
        _sp = GetComponent<SpriteRenderer>();
    }

    //判定
    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Wall")
        {
            _sp.color = Color.red;
            g_bPermissonRange = false;
        }
        else
        {
            _sp.color = Color.green;
            g_bPermissonRange = true;
        }
    }
}
