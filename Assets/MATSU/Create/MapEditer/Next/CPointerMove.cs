﻿using UnityEngine;
using System.Collections;

public class CPointerMove : MonoBehaviour
{
    private GameObject _MainCameraObj = null;
    private GameObject _RangeObj = null;
    private GameObject _PointerObj = null;
    private GameObject _FakeObject = null;

    [SerializeField]
    private float g_fmovespeed = 0.1f;

    //カメラ位置
    private Vector3 _cameraPos;
    //ポインターの位置
    private Vector3 _pointerPos;

    //左右上下の移動限界
    [SerializeField]
    private float g_fMoveMaxX = 0;
    [SerializeField]
    private float g_fMoveMinX = 0;
    [SerializeField]
    private float g_fMoveMaxZ = 0;
    [SerializeField]
    private float g_fMoveMinZ = 0;

    [SerializeField]
    private float g_fZoomMin = 0;
    [SerializeField]
    private float g_fZoomMax = 0;

    
    CUIMove _cuimove = null;
    CCreateCamera _CreateCamera = null;

    //初期化
    void Awake()
    {
        _MainCameraObj =
            GameObject.Find("CreateCamera");
        _RangeObj =
            GameObject.Find("PointRange");
        _PointerObj =
            GameObject.Find("SelectPoint");

        _cuimove = GameObject.Find("CreateMaster").GetComponent<CUIMove>();
        _CreateCamera = _MainCameraObj.GetComponent<CCreateCamera>();
        //位置情報の初期化
        _cameraPos =
            _MainCameraObj.transform.localPosition;
        _pointerPos =
            _PointerObj.transform.localPosition;

    }

    bool xb = false;
    bool zb = false;


    //画面更新
    void Update()
    {
        Zoom();
        if (_FakeObject == null)
        {
            _FakeObject =
                GameObject.Find("FakeObject");
        }

        //ポインターを回転
        if (_FakeObject != null)
        {
            _PointerObj.transform.Rotate(
                new Vector3(0, 1.0f, 0)
                );
            _RangeObj.transform.Rotate(
                new Vector3(0, 0, 1.2f)
                );
            //サンプルを回転させる
            _FakeObject.transform.
                Rotate(0, 0.5f, 0);
        }

        //戻る（実際はBボタン）
        if (Input.GetButton("ButtonAPC"))
        {
            Destroy(_FakeObject);
            //タグを切り替える
            tag = "Select";
            _FakeObject = null;
            _cuimove.enabled = true;
            _CreateCamera.enabled = true;
            enabled = false;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Destroy(_FakeObject);
            //タグを切り替える
            tag = "Select";
            _FakeObject = null;
            _cuimove.enabled = true;
            _CreateCamera.enabled = true;
            enabled = false;
        }

        //何かしらの移動キーを押したらカメラを移動

        float x = Input.GetAxisRaw("HorizontalPC");
        float z = Input.GetAxisRaw("VerticalPC");
        if(x != 0)xb = true;
        else if(x == 0)xb = false;
        if (z != 0) zb = true;
        else if (z == 0) zb = false;

        if (xb || zb)
        {
            _MainCameraObj.transform.localPosition =
                  new Vector3(_pointerPos.x,
                  _cameraPos.y,
                  _pointerPos.z);
            Move();
        }
        if (Input.GetKey(KeyCode.A) |
            Input.GetKey(KeyCode.W) |
            Input.GetKey(KeyCode.S) |
            Input.GetKey(KeyCode.D))
        {
            _MainCameraObj.transform.localPosition =
                new Vector3(_pointerPos.x,
                _cameraPos.y,
                _pointerPos.z);
            Move();
       }

    }

    //移動
    private void Move()
    {
        //位置を代入・ポインターを回転
        _PointerObj.transform.localPosition = _pointerPos;

        //ポインターとカメラの移動
        //上下
        float z = Input.GetAxisRaw("VerticalPC");
        if (_pointerPos.z <= g_fMoveMaxZ)
        {
            _pointerPos.z += z * g_fmovespeed;
        }
        else if (_pointerPos.z >= g_fMoveMinZ)
        {
            _pointerPos.z -= z * g_fmovespeed;
        }

        if (_pointerPos.z > g_fMoveMaxZ)
        {
            _pointerPos.z = g_fMoveMaxZ;
        }
        else if (_pointerPos.z < g_fMoveMinZ)
        {
            _pointerPos.z = g_fMoveMinZ;
        }

        float x = Input.GetAxisRaw("HorizontalPC");
        if (_pointerPos.x <= g_fMoveMaxX)
        {
            _pointerPos.x += x * g_fmovespeed;
        }
        else if (_pointerPos.x >= g_fMoveMinX)
        {
            _pointerPos.x -= x * g_fmovespeed;
        }

        if (_pointerPos.x > g_fMoveMaxX)
        {
            _pointerPos.x = g_fMoveMaxX;
        }
        else if (_pointerPos.x < g_fMoveMinX)
        {
            _pointerPos.x = g_fMoveMinX;
        }


        if (Input.GetKey(KeyCode.W))
        {
            if (_pointerPos.z <= g_fMoveMaxZ)
            {
                _pointerPos.z += 0.1f;
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (_pointerPos.z >= g_fMoveMinZ)
            {
                _pointerPos.z -= 0.1f;
            }
        }

        //左右
        if (Input.GetKey(KeyCode.A))
        {
            if (_pointerPos.x >= g_fMoveMinX)
            {
                _pointerPos.x -= 0.1f;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (_pointerPos.x <= g_fMoveMaxX)
            {
                _pointerPos.x += 0.1f;
            }
        }
    }

    void OnTrigerEnter(Collider coll)
    {
        if (coll.tag == "Floor")
        {
            //つくれる
        }
    }

    void Zoom()
    {
        if (Input.GetButton("ButtonYPC"))
        {
            if (_cameraPos.y >= g_fZoomMin)
            {
                _cameraPos.y -= 0.2f;
            }
        }
        if (Input.GetButton("ButtonXPC"))
        {
            if (_cameraPos.y <= g_fZoomMax)
            {
                _cameraPos.y += 0.2f;
            }
        }
        //PC用
        //ズームイン
        if (Input.GetKey(KeyCode.R))
        {
            if (_cameraPos.y >= g_fZoomMin)
            {
                _cameraPos.y -= 0.2f;
            }
        }

        //ズームアウト
        if (Input.GetKey(KeyCode.F))
        {
            if (_cameraPos.y <= g_fZoomMax)
            {
                _cameraPos.y += 0.2f;
            }
        }
    }
}
