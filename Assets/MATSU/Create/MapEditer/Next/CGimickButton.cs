﻿using UnityEngine;
using System.Collections;

public class CGimickButton : MonoBehaviour
{
    [SerializeField]
    private GameObject _SetObject = null;
    //ギミック配置のスクリプト
    private CUIMove _cUIMoveSc = null;
    private CCreateGimick _cCreateGimick = null;
    private CCreateCamera _CreateCamera = null;
    private CPointerMove _PointerMove = null;

    //初期化
    void Awake()
    {
        //ギミック生成オブジェクトを探す
        _cUIMoveSc = 
            GameObject.Find("CreateMaster").
            GetComponent<CUIMove>();
        _cCreateGimick =
            GameObject.Find("SelectPoint").
            GetComponent<CCreateGimick>();
        _CreateCamera = GameObject.Find("CreateCamera").GetComponent<CCreateCamera>();

        _PointerMove =
      GameObject.Find("CreateMaster").
      GetComponent<CPointerMove>();

    }

    //複製対象のギミック切り替え処理
    void SettingObject()
    {
        _cCreateGimick.enabled = true;
        _cCreateGimick.Create1 = true;
        _cUIMoveSc._createObj = 
            _SetObject;
        _cCreateGimick._SetGimick =
            _SetObject;
        _cUIMoveSc.enabled = false;
        _CreateCamera.enabled = false;
        _PointerMove.enabled = true;
    }
}
