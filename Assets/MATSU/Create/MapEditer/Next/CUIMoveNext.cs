﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CUIMoveNext : MonoBehaviour
{
    [SerializeField]
    private GameObject _selectObj = null;
    [SerializeField]
    private GameObject[] _targetObj = null;
    public GameObject _createObj = null;

    private int g_iselect = 0;

    //色を変更する
    private Image _selectMaterial = null;
    private float g_fAngle = 0;

    //クリエイト側
    [SerializeField]
    private Material _mat;
    private GameObject _FakeObject;
    private GameObject _CameraObj;

    //初期化
    void Awake()
    {
        g_iselect = 0;
        _selectMaterial =
            _selectObj.GetComponent<Image>();
        _CameraObj =
            GameObject.Find("MainCamera");
    }

    //毎フレームごとの更新
    void Update()
    {
        //選択してるオブジェクトに目印をつける
        _selectObj.transform.position =
            _targetObj[g_iselect].transform.position;

        //色を選択してるっぽく
        g_fAngle += 0.02f;
        float fAlpha = Mathf.Abs(Mathf.Sin(g_fAngle));
        //色変更
        _selectMaterial.color =
            new Color(_selectMaterial.color.r,
            _selectMaterial.color.g,
            _selectMaterial.color.b,
            fAlpha
            );

        //タグによって捜査範囲を変更
        if (tag == "Select")
        {
            Select();
        }
    }

    //選択場所の移動
    void Select()
    {
        //キーで選択部分を移動
        if (Input.GetKeyDown(KeyCode.B) //上
            | Input.GetKeyDown(KeyCode.J))
        {
            g_iselect--;
        }
        if (Input.GetKeyDown(KeyCode.M) //下
            | Input.GetKeyDown(KeyCode.N))
        {
            g_iselect++;
        }

        //選択
        if (Input.GetKeyDown(KeyCode.K)) //ボタンA
        {
            _targetObj[g_iselect].
                SendMessage("SettingObject");

            //サンプル表示
            _FakeObject =
                Instantiate(_createObj,
                new Vector3(0, 1000, 3),
                _createObj.transform.rotation) as GameObject;

            _FakeObject.name = "FakeObject";

            //タグを変える
            tag = "Create";
        }

        //行きすぎたら原点へ
        if (g_iselect > 5) g_iselect = 0;
        if (g_iselect < 0) g_iselect = 5;
    }
}
