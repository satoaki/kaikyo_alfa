﻿using UnityEngine;
using System.Collections;

public class CEditManager : MonoBehaviour
{
    //偽物オブジェクト
    private GameObject _FakeObject = null;
    //ギミック説明のパネル
    private GameObject _DescriptionObj = null;
    [SerializeField]
    private GameObject _PointerObj = null;

    private CPointerMove _cPonterMove;
    private CCreateCamera _cCamera;
    private CUIMove _cUIMove;

    //初期化
    void Awake()
    {
        _DescriptionObj =
            GameObject.Find("DescriptionPanel");
        _PointerObj =
            GameObject.Find("SelectPoint");

        _cPonterMove = GetComponent<CPointerMove>();
        _cCamera = GameObject.Find("CreateCamera").GetComponent<CCreateCamera>();
        _cUIMove = GameObject.Find("CreateCamera").GetComponent<CUIMove>();

        _cPonterMove.enabled = false;
        _cCamera.enabled = true;
    }

    //画面更新
    void Update()
    {
        if (_FakeObject == null)
        {
            _FakeObject =
                GameObject.Find("FakeObject");
        }

        if (tag == "Select")
        {
            _PointerObj.SetActive(false);
            _DescriptionObj.SetActive(true);

            _cPonterMove.enabled = false;
            _cCamera.enabled = true;
            _cUIMove.enabled = true;
        }
        if (tag == "Create")
        {
            _PointerObj.SetActive(true);
            _DescriptionObj.SetActive(false);

            _cPonterMove.enabled = true;
            _cCamera.enabled = false;
            _cUIMove.enabled = false;
        }
    }
}
