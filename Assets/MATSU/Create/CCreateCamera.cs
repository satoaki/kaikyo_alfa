﻿using UnityEngine;
using System.Collections;

public class CCreateCamera : MonoBehaviour {

    [SerializeField]
    private float g_fZoomMin = 0;
    [SerializeField]
    private float g_fZoomMax = 0;

    //左右上下の移動限界
    [SerializeField]
    private float g_fMoveMinX = 0;
    [SerializeField]
    private float g_fMoveMaxX = 0;
    [SerializeField]
    private float g_fMoveMinZ = 0;
    [SerializeField]
    private float g_fMoveMaxZ = 0;

    [SerializeField]
    private float g_fmovespeed = 0.2f;

    //ズーム量
    private float g_fzoom = 0;
    //移動量
    private float g_fmoveX = 0;
    private float g_fmoveZ = 0;

    //初期化
    void Awake()
    {
        //位置の初期化
        g_fzoom =
            transform.localPosition.y;
        g_fmoveX =
            transform.localPosition.x;
        g_fmoveZ =
            transform.localPosition.z;
    }

    //画面更新
    void Update()
    {
        //座標に代入
        transform.localPosition =
            new Vector3(g_fmoveX, g_fzoom, g_fmoveZ);

        //ズーム
        Zoom();
        //カメラ移動
        CameraMove();
    }

    //カメラ移動
    void CameraMove()
    {
        //上下
        float z = Input.GetAxisRaw("VerticalPC");
        if (g_fmoveZ <= g_fMoveMaxZ)
        {
            g_fmoveZ += z * g_fmovespeed;
        }
        else if (g_fmoveZ >= g_fMoveMinZ)
        {
            g_fmoveZ -= z * g_fmovespeed;
        }

        if (g_fmoveZ > g_fMoveMaxZ)
        {
            g_fmoveZ = g_fMoveMaxZ;
        }
        else if (g_fmoveZ < g_fMoveMinZ)
        {
            g_fmoveZ = g_fMoveMinZ;
        }
        //左右
        float x = Input.GetAxisRaw("HorizontalPC");
        if (g_fmoveX <= g_fMoveMaxX)
        {
            g_fmoveX += x * g_fmovespeed;
        }
        else if (g_fmoveX >= g_fMoveMinX)
        {
            g_fmoveX -= x * g_fmovespeed;
        }

        if (g_fmoveX > g_fMoveMaxX)
        {
            g_fmoveX = g_fMoveMaxX;
        }
        else if (g_fmoveX < g_fMoveMinX)
        {
            g_fmoveX = g_fMoveMinX;
        }
    }

    //ズーム処理
    void Zoom()
    {
        if (Input.GetButton("ButtonYPC"))
        {
            if (g_fzoom >= g_fZoomMin)
            {
                g_fzoom -= 0.2f;
            }
        }
        if (Input.GetButton("ButtonXPC"))
        {
            if (g_fzoom <= g_fZoomMax)
            {
                g_fzoom += 0.2f;
            }
        }
        //PC用
        //ズームイン
        if (Input.GetKey(KeyCode.R))
        {
            if (g_fzoom >= g_fZoomMin)
            {
                g_fzoom -= 0.2f;
            }
        }

        //ズームアウト
        if (Input.GetKey(KeyCode.F))
        {
            if (g_fzoom <= g_fZoomMax)
            {
                g_fzoom += 0.2f;
            }
        }
    }
}
