﻿using UnityEngine;
using System.Collections;

public class CPageChange : MonoBehaviour
{
    private GameObject _RightArrowObj;
    private GameObject _LeftArrowObj;

    [SerializeField,
        Range(0.1f, 0.5f),
        Tooltip("移動速度")]
    private float speed = 0.0f;

    //初期化
    void Awake()
    {
        _LeftArrowObj =
            GameObject.Find("ArrowL");
        _RightArrowObj =
            GameObject.Find("ArrowR");
    }

    //画面更新
    void Update()
    {
        //矢印のうごき
        ArrowMove();
    }

    //矢印のうごき
    private void ArrowMove()
    {
        _LeftArrowObj.transform.Translate(-speed, 0, 0);
        if (_LeftArrowObj.transform.localPosition.x <= -210)
        {
            _LeftArrowObj.transform.localPosition =
                new Vector3(-190, 0, 0);
        }

        _RightArrowObj.transform.Translate(speed, 0, 0);
        if (_RightArrowObj.transform.localPosition.x >= 210)
        {
            _RightArrowObj.transform.localPosition =
                new Vector3(190, 0, 0);
        }
    }
}
